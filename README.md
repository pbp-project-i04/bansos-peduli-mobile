# bansos-peduli-mobile
[![pipeline status](https://gitlab.com/pbp-project-i04/bansos-peduli-mobile/badges/main/pipeline.svg)](https://gitlab.com/pbp-project-i04/bansos-peduli-mobile/-/commits/main)
## GROUP MEMBERS:
- Anne Yuliana 
- Abdul Mughni Wibisono
- David Julius Vos
- Johanes Christian Lewi Putrael Tarigan
- Raihan Fadhila Sukmana
- Rashad Aziz
- Winaldo Amadea Hestu

## APK LINK: https://www.dropbox.com/s/0g4p8vmgdkbpbjf/bansospeduli-mobile.apk?dl=0

## List of Modules:
### - Home Screen
### - Register Page
### - Login Page
### - User Dashboard
        - MyBansos Page
        - Bansos History
### - Admin Dashboard
        - Generator
            - Distribution Page
            - Register Area Page
            - Bansos Details Page
            - Register Bansos Page
        - Tracking
            - Tracking Page
            - Update Location Page
            - Previous Bansos Location Page
        - Customer-service
            - User Reports Page
### Reports Dashboard
        - List of Reports Page
        - Create Report Page

In terms of integrating our mobile app with the web service (served using django), we plan to utilize the following APIs.
1. Covid API Indonesia \
The API will be fetched from https://api.kawalcorona.com/indonesia through django, which will then be returned as JSON response to be accessed from our mobile app.

2. Authentication \
The information retrieved from the login that is done through the login page of the mobile app will be sent and authenticated in the web service. The handling function will return a JSONResponse that consist of the user information if it exists in the database. This information will be used for authorization for accessing different page of the app.

3. User API \
The User API contains the corresponding information of each user and can be used as end point to manipulate the data in the database. This API will be used for both citizens and staff members. 

4. Bansos API, Distribution API, Report API \
BansosAPI, DistributionAPI, Report API will be used to display bansos, distribution, and report information stored in the database in their respective pages. (For example Distribution Dashboard, Tracking Admin Dashboard). Through this APIs, we can perform full CRUD operation through the GET and POST method.









