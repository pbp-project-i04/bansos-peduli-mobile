import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';

class Laporan {
  String id;
  bool status;
  Map? laporanDetails;

  Laporan({required this.id, required this.status});

  Future<Map> fetchLaporanData() async {
    try {
      var reponse = await get(Uri.parse(
          "https://bansos-peduli.herokuapp.com/laporan/flutter/get/$id/"));
      Map data = jsonDecode(reponse.body);
      String cooldown = data["cooldown"];
      data["cooldown"] = parseDate(cooldown);

      laporanDetails = data;

      return data;
    } catch (e) {
      return Future.error(e);
    }
  }

  @override
  String toString() {
    // TODO: implement toString
    return id;
  }

  DateTime parseDate(String pythonDate) {
    String month = pythonDate.substring(0, 2);
    String day = pythonDate.substring(3, 5);
    String year = pythonDate.substring(6, 10);
    String timestamp = pythonDate.substring(12);
    String dartDate = "$year-$month-$day $timestamp";

    return DateTime.parse(dartDate);
  }
}
