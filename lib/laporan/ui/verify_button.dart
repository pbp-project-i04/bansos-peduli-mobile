import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:bansospedulimobile/laporan/models/laporan.dart';

enum ButtonState { loading, pressed, nothing }

class VerifyReport extends StatefulWidget {
  final Laporan laporan;
  final bool verifying;
  const VerifyReport({Key? key, required this.laporan, required this.verifying})
      : super(key: key);

  Future<void> patchUserLaporan(String laporanid, bool status) async {
    try {
      var response = await post(
          Uri.parse(
              "https://bansos-peduli.herokuapp.com/laporan/flutter/patch/admin/$laporanid/"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode({"status": status}));
      print("hello");
      return Future.delayed(Duration(seconds: 2), () => {});
    } catch (e) {
      throw HttpException("Web Service is Offline");
    }
  }

  @override
  _VerifyReportState createState() => _VerifyReportState();
}

class _VerifyReportState extends State<VerifyReport> {
  ButtonState buttonState = ButtonState.nothing;

  @override
  Widget build(BuildContext context) {
    bool loading = buttonState == ButtonState.loading;
    bool pressed = buttonState == ButtonState.pressed;
    bool notPressed = buttonState == ButtonState.nothing;
    final size = MediaQuery.of(context).size;
    return WillPopScope(
        child: Container(
            margin: const EdgeInsets.only(top: 10),
            child: InkWell(
                customBorder: loading
                    ? CircleBorder()
                    : RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                onTap: () async {
                  setState(() {
                    buttonState = ButtonState.pressed;
                    buttonState = ButtonState.loading;
                  });
                  try {
                    await widget.patchUserLaporan(
                        widget.laporan.id, widget.verifying);
                    Navigator.of(context).pop(true);
                  } catch (e) {
                    setState(() {
                      buttonState = ButtonState.nothing;
                    });
                  }
                },
                child: Container(
                  decoration: pressed || loading
                      ? BoxDecoration(
                          shape: BoxShape.circle,
                          color: widget.verifying ? Colors.green : Colors.red)
                      : BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: widget.verifying ? Colors.green : Colors.red),
                  width: notPressed ? size.width * 0.4 : 45,
                  height: 45,
                  child: notPressed
                      ? Center(
                          child: widget.verifying
                              ? const Text(
                                  "Verify",
                                  style: TextStyle(color: Colors.white),
                                )
                              : const Text("Unverify",
                                  style: TextStyle(color: Colors.white)),
                        )
                      : loading
                          ? const Center(
                              child: CircularProgressIndicator(
                              color: Colors.white,
                            ))
                          : Container(),
                ))),
        onWillPop: () async {
          return !loading;
        });
  }
}
