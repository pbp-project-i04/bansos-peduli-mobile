import 'package:flutter/material.dart';

class CustomTextBox extends StatelessWidget {
  final String text;
  const CustomTextBox({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: text,
      enabled: false,
      style: TextStyle(fontSize: 16),
      maxLines: 10,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          disabledBorder:
              OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          errorText: null,
          fillColor: Colors.blue[100],
          hintMaxLines: 10,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))),
    );
  }
}
