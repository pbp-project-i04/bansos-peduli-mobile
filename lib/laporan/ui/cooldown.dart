import 'dart:async';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/laporan/models/laporan.dart';

class CooldownCountdown extends StatefulWidget {
  VoidCallback? onPressedWhenCooldownFinished;
  final DateTime cooldown;
  CooldownCountdown(
      {Key? key, required this.cooldown, this.onPressedWhenCooldownFinished})
      : super(key: key);

  @override
  _CooldownCountdownState createState() => _CooldownCountdownState();
}

class _CooldownCountdownState extends State<CooldownCountdown> {
  bool _cooldownFinished = false;
  String? countdown;
  Timer? _timer;
  @override
  void initState() {
    // TODO: implement initState
    updateCountdown();

    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      updateCountdown();
    });
  }

  updateCountdown() {
    DateTime currentDate = DateTime.now();
    int days = widget.cooldown.difference(currentDate).inDays;
    int hours = widget.cooldown.difference(currentDate).inHours % 24;
    int minutes = widget.cooldown.difference(currentDate).inMinutes % 60;
    int seconds = widget.cooldown.difference(currentDate).inSeconds % 60;

    if (widget.cooldown.isBefore(currentDate)) {
      if (_timer != null) {
        _timer!.cancel();
        _timer = null;
      }
      setState(() {
        _cooldownFinished = true;
      });
      return;
    }

    if (days >= 1) {
      setState(() {
        countdown = "$days days";
      });
    } else if (hours <= 23 && hours > 0) {
      setState(() {
        countdown =
            "${hours.toString().padLeft(2, '0')}:${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}";
      });
    } else if (minutes <= 60 && minutes > 0) {
      setState(() {
        countdown =
            "${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}";
      });
    } else if (seconds <= 60 && seconds > 0) {
      setState(() {
        countdown = "${seconds.toString().padLeft(2, '0')} seconds";
      });
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.4,
        height: 60,
        child: _cooldownFinished
            ? InkWell(
                onTap: widget.onPressedWhenCooldownFinished,
                splashColor: Colors.blue,
                child: Card(
                  color: Colors.blue[900],
                  child: const Center(
                    child: Text("Update Report",
                        style: TextStyle(color: Colors.white, fontSize: 18)),
                  ),
                ),
              )
            : Card(
                color: Colors.blue.shade300,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Update Report",
                        style: TextStyle(color: Colors.grey.shade300),
                      ),
                      RichText(
                        text: TextSpan(children: [
                          WidgetSpan(
                              child: Icon(
                            Icons.lock,
                            size: 14,
                            color: Colors.grey.shade300,
                          )),
                          TextSpan(
                              text: " $countdown",
                              style: TextStyle(color: Colors.grey.shade300))
                        ]),
                      )
                    ],
                  ),
                ),
              ));
  }
}
