import 'package:bansospedulimobile/user/models/user.dart';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/laporan/ui/textbox.dart';

class DetailLaporan extends StatelessWidget {
  final User user;
  final Map laporanDetails;
  const DetailLaporan(
      {Key? key, required this.laporanDetails, required this.user})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text("${user.isAdmin ? 'User' : 'Your'} Submitted Report",
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline,
                fontSize: 20,
              )),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "ID: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "${laporanDetails["laporan_id"]}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Category: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text:
                        "${laporanDetails["report_category"][0].toUpperCase() + laporanDetails["report_category"].substring(1)}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "${user.isAdmin ? 'User' : 'Your'} Report Message: ",
              style: const TextStyle(fontWeight: FontWeight.bold))),
        ),
        CustomTextBox(text: laporanDetails["report_message"]),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 8, bottom: 3),
          child: const Text("Reported Bansos Info",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline,
                fontSize: 18,
              )),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Responsible Admin: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text:
                        "${laporanDetails["bansos_info"]['responsible_admin']}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Provinsi: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "${laporanDetails["bansos_info"]['provinsi']}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Kecamatan: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "${laporanDetails["bansos_info"]['kecamatan']}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Kelurahan: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "${laporanDetails["bansos_info"]['kelurahan']}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Arrived On: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "${laporanDetails["bansos_info"]['timestamp']}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: const Text("Bentuk Bantuan",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline,
                fontSize: 18,
              )),
        ),
        Container(
          decoration: BoxDecoration(border: Border.all(width: 0.25)),
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: DataTable(
            headingRowColor: MaterialStateProperty.all(Colors.blue[300]),
            headingRowHeight: 45,
            dataRowColor: MaterialStateProperty.all(Colors.grey[200]),
            dividerThickness: 2,
            showBottomBorder: true,
            columns: const [
              DataColumn(label: Text("Bentuk")),
              DataColumn(label: Text("Jumlah"))
            ],
            rows: [
              ...laporanDetails["bansos_info"]["bentuk_bantuan"]
                  .entries
                  .map((bentuk) {
                var bentukBantuanData =
                    laporanDetails["bansos_info"]["bentuk_bantuan"][bentuk.key];
                var jumlah = bentukBantuanData["jumlah"];
                var satuan = bentukBantuanData["satuan"];
                return DataRow(cells: [
                  DataCell(Text("${bentuk.key}")),
                  DataCell(Text("$jumlah $satuan")),
                ]);
              })
            ],
          ),
        )
      ],
    );
  }
}
