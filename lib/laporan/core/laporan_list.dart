import 'dart:convert';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/laporan/models/laporan.dart';
import 'package:http/http.dart';

import 'laporan_details.dart';

Future<List<Laporan>> getUserLaporan(String sessionid) async {
  try {
    var response = await get(Uri.parse(
        "https://bansos-peduli.herokuapp.com/laporan/flutter/get/all/$sessionid/"));

    if (response.statusCode == 200) {
      Map<String, dynamic> rawData = jsonDecode(response.body);
      List<Laporan> allLaporan = [];

      for (String id in rawData.keys) {
        Laporan laporan = Laporan(id: id, status: rawData[id]);
        allLaporan.add(laporan);
        // laporan.fetchLaporanData();
      }
      return allLaporan;
    } else {
      return Future.error("error");
    }
  } catch (e) {
    return Future.error("error");
  }
}

class LaporanList extends StatefulWidget {
  final User user;

  const LaporanList({Key? key, required this.user}) : super(key: key);

  @override
  _LaporanListState createState() => _LaporanListState();
}

class _LaporanListState extends State<LaporanList> {
  List<bool> _selections = [true, false];
  var allLaporan;
  @override
  void initState() {
    allLaporan = getUserLaporan(widget.user.sessionId);
    super.initState();
  }

  Future<void> refreshPage() async {
    setState(() {
      allLaporan = getUserLaporan(widget.user.sessionId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
          title: Text(
            "All Your Previous Reports",
            style: TextStyle(color: Theme.of(context).primaryColor),
          ),
          backgroundColor: Colors.white,
          foregroundColor: Colors.blue,
          elevation: 0,
        ),
        body: Column(
          children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(bottom: 10),
                width: 166,
                height: 40,
                child: ToggleButtons(
                  borderWidth: 2,
                  color: Colors.blue,
                  borderColor: Theme.of(context).primaryColor,
                  selectedBorderColor: Colors.blue,
                  fillColor: Theme.of(context).primaryColor,
                  selectedColor: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  children: [
                    Container(
                      width: 80,
                      child: const Text(
                        "Accepted",
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      width: 80,
                      child: const Text(
                        "Pending",
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                  isSelected: _selections,
                  onPressed: (index) {
                    setState(() {
                      _selections[0] = false;
                      _selections[1] = false;
                      _selections[index] = true;
                    });
                  },
                ),
              ),
            ),
            Expanded(
                child: RefreshIndicator(
              child: FutureBuilder(
                future: allLaporan,
                builder: (context, snapshot) {
                  if (snapshot.connectionState != ConnectionState.done) {
                    return const Center(
                      child: CircularProgressIndicator(color: Colors.blue),
                    );
                  } else if (!snapshot.hasError) {
                    List<Laporan> laporanId = snapshot.data as List<Laporan>;
                    List<Laporan> acceptedLaporan = [];
                    List<Laporan> pendingLaporan = [];

                    for (Laporan laporan in laporanId) {
                      if (laporan.status) {
                        acceptedLaporan.add(laporan);
                      } else {
                        pendingLaporan.add(laporan);
                      }
                    }

                    return _selections[0]
                        ? acceptedLaporan.isEmpty
                            ? Center(
                                child: Column(
                                children: [
                                  const Icon(
                                    Icons.report_off_sharp,
                                    size: 100,
                                    color: Colors.blue,
                                  ),
                                  const Text(
                                      "Seems like none of your reports have been accepted"),
                                  IconButton(
                                      onPressed: refreshPage,
                                      icon: const Icon(Icons.refresh_sharp))
                                ],
                              ))
                            : ListView.builder(
                                itemCount: acceptedLaporan.length,
                                itemBuilder: (context, index) {
                                  // display something if there are no accepted/pending laporan
                                  return Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      elevation: 5,
                                      child: ListTile(
                                        tileColor:
                                            Theme.of(context).primaryColor,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        subtitle:
                                            const Text("click to view details"),
                                        title: Text(
                                            "REPORT ${acceptedLaporan[index]}",
                                            style: const TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white)),
                                        trailing: const Icon(
                                          Icons.arrow_right,
                                          color: Colors.white,
                                        ),

                                        onTap: () => {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      LaporanDetails(
                                                          user: widget.user,
                                                          laporan:
                                                              acceptedLaporan[
                                                                  index])))
                                        }, // route to details page based on the laporanId
                                      ));
                                })
                        : pendingLaporan.isEmpty
                            ? Center(
                                child: Column(
                                children: [
                                  const Icon(
                                    Icons.report_off_sharp,
                                    size: 100,
                                    color: Colors.blue,
                                  ),
                                  const Text(
                                      "Seems like you have no pending reports"),
                                  IconButton(
                                      onPressed: () {
                                        try {
                                          refreshPage();
                                        } catch (e) {
                                          return;
                                        }
                                      },
                                      icon: const Icon(Icons.refresh_sharp))
                                ],
                              ))
                            : ListView.builder(
                                itemCount: pendingLaporan.length,
                                itemBuilder: (context, index) {
                                  return Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      elevation: 5,
                                      child: ListTile(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        subtitle: Text("click to view details"),
                                        title: Text(
                                            "REPORT ${pendingLaporan[index]}",
                                            style: const TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white)),
                                        trailing: const Icon(
                                          Icons.arrow_right,
                                          color: Colors.white,
                                        ),
                                        tileColor:
                                            Theme.of(context).primaryColor,
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      LaporanDetails(
                                                          user: widget.user,
                                                          laporan:
                                                              pendingLaporan[
                                                                  index])));
                                        }, // route to details page based on the laporanId
                                      ));
                                });
                  } else {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.signal_wifi_connected_no_internet_4_sharp,
                            size: 100,
                            color: Colors.blue,
                          ),
                          const Text(
                            "Something went wrong :(",
                            style: TextStyle(color: Colors.blue),
                          ),
                          IconButton(
                              onPressed: () {
                                Navigator.pushReplacement(
                                  context,
                                  PageRouteBuilder(
                                    pageBuilder:
                                        (context, animation1, animation2) =>
                                            LaporanList(
                                      user: widget.user,
                                    ),
                                    transitionDuration: Duration.zero,
                                  ),
                                );
                              },
                              icon: const Icon(Icons.refresh)),
                        ],
                      ),
                    );
                  }
                },
              ),
              onRefresh: refreshPage,
            )),
          ],
        ));
  }
}
