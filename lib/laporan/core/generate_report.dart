import 'dart:convert';
import 'package:bansospedulimobile/laporan/bansos_report.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:http/http.dart';
import 'package:bansospedulimobile/laporan/models/laporan.dart';
import 'package:bansospedulimobile/laporan/ui/custom_radio.dart';

Future<void> postReport(String reportText, String category, String bansosID,
    String sessionId) async {
  try {
    var response = await post(
        Uri.parse(
            "https://bansos-peduli.herokuapp.com/laporan/flutter/post/$sessionId/"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          "report_message": reportText,
          "report_category": category,
          "reported_bansos_id": bansosID
        }));

    if (response.statusCode == 201) {
      return;
    } else {
      return Future.error("Web Service is Offline");
    }
  } catch (e) {
    return Future.error("error");
  }
}

Future<void> patchReport(
    String reportText, String category, String laporanId) async {
  try {
    var response = await post(
        Uri.parse(
            "https://bansos-peduli.herokuapp.com/laporan/flutter/patch/$laporanId/"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          "report_message": reportText,
          "report_category": category,
        }));
    if (response.statusCode == 204) {
      return;
    } else {
      return Future.error("Web Service is Offline");
    }
  } catch (e) {
    return Future.error("error");
  }
  // return Future.delayed(Duration(seconds: 2));
}

class GenerateReport extends StatefulWidget {
  final User user;
  final String bansosID;
  final Laporan? laporan;
  final bool isUpdating;

  const GenerateReport(
      {Key? key,
      required this.bansosID,
      this.laporan,
      required this.isUpdating,
      required this.user})
      : super(key: key);

  @override
  _GenerateReportState createState() => _GenerateReportState();
}

class _GenerateReportState extends State<GenerateReport> {
  String? _reportText;
  String? radioValue;
  bool _errorRadio = false;
  bool _submitting = false;
  bool shouldPop = true;

  final radioKey = GlobalKey();
  final formKey = GlobalKey<FormState>();
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    if (widget.isUpdating) {
      _reportText = widget.laporan!.laporanDetails!["report_message"];
      radioValue = widget.laporan!.laporanDetails!["report_category"];
    }
    super.initState();
  }

  void radioError() {
    setState(() {
      _errorRadio = true;
    });
  }

  void radioResolved() {
    setState(() {
      _errorRadio = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return WillPopScope(
        child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: Scaffold(
                backgroundColor: Colors.white,
                appBar: AppBar(
                  foregroundColor: Colors.blue,
                  backgroundColor: Colors.white,
                  elevation: 0,
                  title: widget.isUpdating
                      ? Text("Updating Report Bansos ID#${widget.bansosID}")
                      : Text("Report Bansos ID#${widget.bansosID}"),
                ),
                body: SingleChildScrollView(
                  child: Center(
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10, bottom: 5),
                          width: size.width * 0.8,
                          child: const Text(
                              "Click to Choose A Report Category:",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16)),
                        ),
                        _errorRadio
                            ? Container(
                                child: const Text(
                                  "Please select a report category",
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 12),
                                ),
                              )
                            : Container(),
                        CustomRadio(
                          key: radioKey,
                          initialGroupValue: radioValue,
                          enabled: _submitting ? false : true,
                          radioOneTitle: "Product Expired",
                          radioOneValue: "expired",
                          radioTwoTitle: "Product Insufficient",
                          radioTwoValue: "insufficient",
                          radioThreeTitle: "Other",
                          radioThreeValue: "other",
                          borderRadius: 10,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10, bottom: 10),
                          width: size.width * 0.8,
                          child: const Text("Your Message:",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20)),
                        ),
                        Container(
                            width: size.width * 0.8,
                            child: Form(
                              key: formKey,
                              child: TextFormField(
                                initialValue: _reportText,
                                enabled: _submitting ? false : true,
                                style: TextStyle(fontSize: 18),
                                maxLines: 10,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    errorText: null,
                                    fillColor: Colors.blue[100],
                                    hintMaxLines: 10,
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(10))),
                                validator: (value) {
                                  return value == null ||
                                          value.isEmpty ||
                                          value.trim() == ""
                                      ? "Please Fill in the Field"
                                      : null;
                                },
                                onSaved: (value) => setState(() {
                                  _reportText = value;
                                }),
                              ),
                            )),
                        Container(
                            margin: EdgeInsets.only(top: 5),
                            width: size.width * 0.3,
                            child: ElevatedButton(
                                onPressed: () {
                                  FormState? formState = formKey.currentState;
                                  var radioState = radioKey.currentState;

                                  if (formState!.validate() &&
                                      "$radioState" != "null") {
                                    radioResolved();
                                    formState.save();
                                    showModalBottomSheet(
                                        context: context,
                                        builder: (context) {
                                          return Container(
                                            height: size.height * 0.3,
                                            color: Colors.white,
                                            child: Center(
                                              child: Column(
                                                children: [
                                                  Container(
                                                      margin: const EdgeInsets
                                                              .symmetric(
                                                          vertical: 10),
                                                      child: const Text(
                                                          "Have You Checked Again?",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.blue,
                                                              fontSize: 20,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold))),
                                                  ElevatedButton(
                                                      onPressed: () {
                                                        setState(() {
                                                          _submitting = true;
                                                          Navigator.pop(
                                                              context);
                                                          shouldPop = false;
                                                        });
                                                      },
                                                      child: const Text(
                                                          "Yes, I Want to Submit My Report")),
                                                  SizedBox(
                                                    child: ElevatedButton(
                                                      style: ElevatedButton
                                                          .styleFrom(
                                                              primary:
                                                                  Colors.grey),
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                      child: const Text(
                                                          "No, I Want to Check Again"),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        });
                                  } else if (!(formState.validate()) &&
                                      "$radioState" != "null") {
                                    radioResolved();
                                  } else {
                                    radioError();
                                  }
                                },
                                child: !_submitting
                                    ? SizedBox(
                                        height: 45,
                                        child: Center(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              widget.isUpdating
                                                  ? const Text(
                                                      "Update Report",
                                                      style: TextStyle(
                                                          fontSize: 13.2),
                                                    )
                                                  : const Text(
                                                      "Send Report",
                                                      style: TextStyle(
                                                          fontSize: 14),
                                                    )
                                            ],
                                          ),
                                        ))
                                    : FutureBuilder(
                                        future: widget.isUpdating
                                            ? patchReport(
                                                _reportText!,
                                                "${radioKey.currentState}",
                                                widget.laporan!.id)
                                            : postReport(
                                                _reportText!,
                                                "${radioKey.currentState}",
                                                widget.bansosID,
                                                widget.user.sessionId),
                                        builder: (context, snapshot) {
                                          if (snapshot.connectionState !=
                                              ConnectionState.done) {
                                            return Container(
                                              padding: const EdgeInsets.all(10),
                                              child: const SizedBox(
                                                width: 30,
                                                height: 30,
                                                child:
                                                    CircularProgressIndicator(
                                                  color: Colors.white,
                                                ),
                                              ),
                                            );
                                          } else if (!snapshot.hasError) {
                                            SchedulerBinding.instance!
                                                .addPostFrameCallback((_) {
                                              Navigator.of(context)
                                                  .pushAndRemoveUntil(
                                                      MaterialPageRoute<void>(
                                                          builder: (BuildContext
                                                                  context) =>
                                                              BansosReportPage(
                                                                user:
                                                                    widget.user,
                                                                justReported:
                                                                    true,
                                                              )),
                                                      (Route<dynamic> route) =>
                                                          false);
                                            });
                                            return const Text("");
                                          } else {
                                            WidgetsBinding.instance!
                                                .addPostFrameCallback(
                                                    (timeStamp) {
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(const SnackBar(
                                                      backgroundColor:
                                                          Colors.grey,
                                                      elevation: 6,
                                                      behavior: SnackBarBehavior
                                                          .floating,
                                                      content: Text(
                                                        "There was a problem in sending the data to the server",
                                                        style: TextStyle(
                                                            color: Colors.blue),
                                                      )));
                                              setState(() {
                                                shouldPop = true;
                                                _submitting = false;
                                              });
                                            });

                                            return Center(
                                              child: widget.isUpdating
                                                  ? const Text(
                                                      "Update Report",
                                                      style: TextStyle(
                                                          fontSize: 13.2),
                                                    )
                                                  : const Text(
                                                      "Send Report",
                                                      style: TextStyle(
                                                          fontSize: 14),
                                                    ),
                                            );
                                          }
                                        })))
                      ],
                    ),
                  ),
                ))),
        onWillPop: () async {
          return shouldPop;
        });
  }
}
