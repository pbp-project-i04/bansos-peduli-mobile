import 'package:bansospedulimobile/laporan/models/bansos.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:bansospedulimobile/laporan/core/bansos_detail_page.dart';
import 'package:flutter/material.dart';
import 'generate_report.dart';

class BansosDetails extends StatefulWidget {
  final User user;
  final Bansos bansos;
  const BansosDetails({Key? key, required this.bansos, required this.user})
      : super(key: key);

  @override
  _BansosDetailsState createState() => _BansosDetailsState();
}

class _BansosDetailsState extends State<BansosDetails> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Details for Bansos ID#${widget.bansos}",
            style: const TextStyle(fontSize: 18),
          ),
          backgroundColor: Colors.grey[50],
          foregroundColor: Colors.blue,
          elevation: 0,
        ),
        body: widget.bansos.bansosDetails != null
            ? SingleChildScrollView(
                child: Container(
                    padding: const EdgeInsets.only(top: 5, left: 20, right: 20),
                    child: Column(
                      children: [
                        DetailBansos(bansos: widget.bansos),
                        Container(
                          margin: const EdgeInsets.only(top: 10),
                          width: width * 0.5,
                          child: ElevatedButton(
                            child: const Text("REPORT"),
                            style:
                                ElevatedButton.styleFrom(primary: Colors.red),
                            onPressed: () => {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => GenerateReport(
                                          user: widget.user,
                                          bansosID: "${widget.bansos}",
                                          isUpdating: false)))
                            },
                          ),
                        ),
                      ],
                    )))
            : FutureBuilder(
                future: widget.bansos.fetchBansosData(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState != ConnectionState.done) {
                    return const Center(
                        child: CircularProgressIndicator(
                      strokeWidth: 5,
                    ));
                  } else if (!snapshot.hasError) {
                    return SingleChildScrollView(
                        child: Container(
                            padding: const EdgeInsets.only(
                                top: 5, left: 20, right: 20),
                            child: Column(
                              children: [
                                DetailBansos(bansos: widget.bansos),
                                Container(
                                  margin: const EdgeInsets.only(top: 10),
                                  width: width * 0.5,
                                  child: ElevatedButton(
                                    child: const Text("REPORT"),
                                    style: ElevatedButton.styleFrom(
                                        primary: Colors.red),
                                    onPressed: () => {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  GenerateReport(
                                                      user: widget.user,
                                                      bansosID:
                                                          "${widget.bansos}",
                                                      isUpdating: false)))
                                    },
                                  ),
                                )
                              ],
                            )));
                  } else {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.signal_wifi_connected_no_internet_4_sharp,
                            size: 100,
                            color: Colors.blue,
                          ),
                          const Text(
                            "Something went wrong :(",
                            style: TextStyle(color: Colors.blue),
                          ),
                          IconButton(
                              onPressed: () {
                                Navigator.pushReplacement(
                                  context,
                                  PageRouteBuilder(
                                    pageBuilder:
                                        (context, animation1, animation2) =>
                                            BansosDetails(
                                      bansos: widget.bansos,
                                      user: widget.user,
                                    ),
                                    transitionDuration: Duration.zero,
                                  ),
                                );
                              },
                              icon: const Icon(Icons.refresh)),
                        ],
                      ),
                    );
                  }
                },
              ));
  }
}
