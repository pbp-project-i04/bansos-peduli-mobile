import 'package:bansospedulimobile/laporan/core/generate_report.dart';
import 'package:bansospedulimobile/laporan/ui/cooldown.dart';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/laporan/models/laporan.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:bansospedulimobile/laporan/core/laporan_detail_page.dart';
import 'package:bansospedulimobile/laporan/ui/verify_button.dart';

class LaporanDetails extends StatefulWidget {
  final User user;
  final Laporan laporan;
  const LaporanDetails({Key? key, required this.laporan, required this.user})
      : super(key: key);

  @override
  _LaporanDetailsState createState() => _LaporanDetailsState();
}

class _LaporanDetailsState extends State<LaporanDetails> {
  Map? laporanDetails;

  @override
  void initState() {
    // TODO: implement initState
    laporanDetails = widget.laporan.laporanDetails;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Details for Laporan ID#${widget.laporan.id}",
          style: const TextStyle(fontSize: 18),
        ),
        backgroundColor: Colors.grey[50],
        foregroundColor: Colors.blue,
        elevation: 0,
      ),
      body: laporanDetails != null
          ? SingleChildScrollView(
              child: Container(
                  padding: const EdgeInsets.only(top: 5, left: 20, right: 20),
                  child: Column(
                    children: widget.laporan.status || widget.user.isAdmin
                        ? [
                            DetailLaporan(
                              user: widget.user,
                              laporanDetails: laporanDetails!,
                            ),
                            widget.user.isAdmin
                                ? VerifyReport(
                                    laporan: widget.laporan,
                                    verifying:
                                        widget.laporan.status ? false : true,
                                  )
                                : Container()
                          ]
                        : [
                            DetailLaporan(
                              user: widget.user,
                              laporanDetails: laporanDetails!,
                            ),
                            CooldownCountdown(
                              cooldown: laporanDetails!["cooldown"],
                              onPressedWhenCooldownFinished: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => GenerateReport(
                                              user: widget.user,
                                              bansosID: laporanDetails![
                                                  "reported_bansos_id"],
                                              isUpdating: true,
                                              laporan: widget.laporan,
                                            )));
                              },
                            )
                          ],
                  )))
          : FutureBuilder(
              future: widget.laporan.fetchLaporanData(),
              builder: (context, snapshot) {
                if (snapshot.connectionState != ConnectionState.done) {
                  return const Center(
                    child: CircularProgressIndicator(color: Colors.blue),
                  );
                } else if (!snapshot.hasError) {
                  laporanDetails = snapshot.data as Map;
                  return SingleChildScrollView(
                      child: Container(
                          padding: const EdgeInsets.only(
                              top: 5, left: 20, right: 20),
                          child: Column(
                            children: widget.laporan.status ||
                                    widget.user.isAdmin
                                ? [
                                    DetailLaporan(
                                      user: widget.user,
                                      laporanDetails: laporanDetails!,
                                    ),
                                    widget.user.isAdmin
                                        ? VerifyReport(
                                            laporan: widget.laporan,
                                            verifying: widget.laporan.status
                                                ? false
                                                : true,
                                          )
                                        : Container()
                                  ]
                                : [
                                    DetailLaporan(
                                        user: widget.user,
                                        laporanDetails: laporanDetails!),
                                    CooldownCountdown(
                                      cooldown: laporanDetails!["cooldown"],
                                      onPressedWhenCooldownFinished: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    GenerateReport(
                                                      user: widget.user,
                                                      bansosID: laporanDetails![
                                                          "reported_bansos_id"],
                                                      isUpdating: true,
                                                      laporan: widget.laporan,
                                                    )));
                                      },
                                    )
                                  ],
                          )));
                } else {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Icon(
                          Icons.signal_wifi_connected_no_internet_4_sharp,
                          size: 100,
                          color: Colors.blue,
                        ),
                        const Text(
                          "Something went wrong :(",
                          style: TextStyle(color: Colors.blue),
                        ),
                        IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(
                                context,
                                PageRouteBuilder(
                                  pageBuilder:
                                      (context, animation1, animation2) =>
                                          LaporanDetails(
                                    laporan: widget.laporan,
                                    user: widget.user,
                                  ),
                                  transitionDuration: Duration.zero,
                                ),
                              );
                            },
                            icon: const Icon(Icons.refresh)),
                      ],
                    ),
                  );
                }
              }),
    );
  }
}
