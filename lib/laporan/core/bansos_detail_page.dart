import 'package:flutter/material.dart';
import 'package:bansospedulimobile/laporan/models/bansos.dart';

class DetailBansos extends StatelessWidget {
  final Bansos bansos;
  const DetailBansos({Key? key, required this.bansos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Responsible Admin: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "${bansos.bansosDetails!['responsible_admin']}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: const Text("Info Bansos",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline,
                fontSize: 20,
              )),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Provinsi: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "${bansos.bansosDetails!['provinsi']}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Kecamatan: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "${bansos.bansosDetails!['kecamatan']}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Kelurahan: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "${bansos.bansosDetails!['kelurahan']}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: Text.rich(TextSpan(
              text: "Arrived On: ",
              style: const TextStyle(fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "${bansos.bansosDetails!['timestamp']}",
                    style: const TextStyle(fontWeight: FontWeight.normal))
              ])),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: const Text("Bentuk Bantuan",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline,
                fontSize: 20,
              )),
        ),
        Container(
          decoration: BoxDecoration(border: Border.all(width: 0.25)),
          width: double.infinity,
          margin: const EdgeInsets.only(top: 3, bottom: 3),
          child: DataTable(
            headingRowColor: MaterialStateProperty.all(Colors.blue[300]),
            headingRowHeight: 45,
            dataRowColor: MaterialStateProperty.all(Colors.grey[200]),
            dividerThickness: 2,
            showBottomBorder: true,
            columns: const [
              DataColumn(label: Text("Bentuk")),
              DataColumn(label: Text("Jumlah"))
            ],
            rows: [
              ...bansos.bansosDetails!["bentuk_bantuan"].entries.map((bentuk) {
                var bentukBantuanData =
                    bansos.bansosDetails!["bentuk_bantuan"][bentuk.key];
                var jumlah = bentukBantuanData["jumlah"];
                var satuan = bentukBantuanData["satuan"];
                return DataRow(cells: [
                  DataCell(Text("${bentuk.key}")),
                  DataCell(Text("$jumlah $satuan")),
                ]);
              })
            ],
          ),
        )
      ],
    );
  }
}
