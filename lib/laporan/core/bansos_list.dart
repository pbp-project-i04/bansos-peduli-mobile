import 'dart:convert';
import 'package:bansospedulimobile/laporan/core/bansos_details.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:async';
import 'package:bansospedulimobile/laporan/models/bansos.dart';

getUserBansos(String sessionid) async {
  try {
    var reponse = await get(Uri.parse(
        "https://bansos-peduli.herokuapp.com/laporan/flutter/get/all/bansos/$sessionid/"));

    var data = jsonDecode(reponse.body)["data"];
    List<Bansos> allUserBansos = [];

    for (var id in data) {
      Bansos bansos = Bansos(id);
      allUserBansos.add(bansos);
      // bansos.fetchBansosData();
    }

    return allUserBansos;
  } catch (e) {
    return Future.error("error");
  }
}

class BansosList extends StatefulWidget {
  final User user;
  const BansosList({Key? key, required this.user}) : super(key: key);

  @override
  _BansosListState createState() => _BansosListState();
}

class _BansosListState extends State<BansosList> {
  Timer? _timer;
  dynamic allBansosID;
  bool refreshed = false;

  Future<void> refreshPage() async {
    setState(() {
      allBansosID = getUserBansos(widget.user.sessionId);
    });
  }

  @override
  void initState() {
    allBansosID = getUserBansos(widget.user.sessionId);

    _timer = Timer.periodic(const Duration(seconds: 8), (timer) {
      checkForUpdates();
    });
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    }
    super.dispose();
  }

  void checkForUpdates() async {
    if (allBansosID != null) {
      try {
        dynamic getCurrentBansos = await allBansosID;
        List<Bansos> currentBansos = getCurrentBansos as List<Bansos>;
        List<Bansos> maybeNewBansos =
            await getUserBansos(widget.user.sessionId);

        bool thereIsNewBansos = currentBansos.length != maybeNewBansos.length;
        if (thereIsNewBansos) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: const Text(
              "You have new Bansos Arriving, Pull down to Refresh",
              style: TextStyle(color: Colors.blue),
            ),
            backgroundColor: Colors.grey,
            behavior: SnackBarBehavior.floating,
            elevation: 6,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          ));
        }
      } catch (e) {
        return;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
          title: Text(
            "Report Your Bansos",
            style: TextStyle(color: Theme.of(context).primaryColor),
          ),
          backgroundColor: Colors.white,
          foregroundColor: Colors.blue,
          elevation: 0,
        ),
        body: RefreshIndicator(
            child: FutureBuilder(
              future: allBansosID,
              builder: (context, snapshot) {
                if (snapshot.connectionState != ConnectionState.done) {
                  return const Center(
                      child: CircularProgressIndicator(
                    strokeWidth: 5,
                    color: Colors.blue,
                  ));
                } else if (snapshot.connectionState == ConnectionState.done &&
                    !snapshot.hasError) {
                  List<Bansos> bansosID = snapshot.data! as List<Bansos>;
                  if (bansosID.isEmpty) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.speaker_notes_off_sharp,
                            size: 100,
                            color: Colors.blue,
                          ),
                          const Text(
                            "Seems Like you have no Bansos to Report",
                            style: TextStyle(color: Colors.blue),
                          ),
                          IconButton(
                              onPressed: refreshed ? null : refreshPage,
                              icon: const Icon(Icons.refresh)),
                        ],
                      ),
                    );
                  }
                  return ListView.builder(
                      itemCount: bansosID.length + 1,
                      itemBuilder: (context, index) {
                        if (index == 0) {
                          return Column(
                            children: [
                              Container(
                                margin: const EdgeInsets.only(top: 20),
                                child: const Text(
                                  "Choose Bansos to Report: ",
                                  style: TextStyle(
                                    color: Colors.blue,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 24,
                                  ),
                                ),
                              ),
                            ],
                          );
                        } else {
                          return Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              elevation: 5,
                              child: ListTile(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                subtitle: const Text("click to view details"),
                                title: Text("BANSOS ${bansosID[index - 1]}",
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white)),
                                trailing: const Icon(
                                  Icons.arrow_right,
                                  color: Colors.white,
                                ),
                                tileColor: Theme.of(context).primaryColor,
                                onTap: () => {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => BansosDetails(
                                              user: widget.user,
                                              bansos: bansosID[index - 1])))
                                }, // route to details page based on the bansosID
                              ));
                        }
                      });
                } else {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Icon(
                          Icons.signal_wifi_connected_no_internet_4_sharp,
                          size: 100,
                          color: Colors.blue,
                        ),
                        const Text(
                          "Something went wrong :(",
                          style: TextStyle(color: Colors.blue),
                        ),
                        IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(
                                context,
                                PageRouteBuilder(
                                  pageBuilder:
                                      (context, animation1, animation2) =>
                                          BansosList(
                                    user: widget.user,
                                  ),
                                  transitionDuration: Duration.zero,
                                ),
                              );
                            },
                            icon: const Icon(Icons.refresh)),
                      ],
                    ),
                  ); // display error sign
                }
              },
            ),
            onRefresh: refreshPage));
  }
}
