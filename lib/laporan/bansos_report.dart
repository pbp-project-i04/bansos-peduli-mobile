import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:bansospedulimobile/beranda/myhomepage.dart';
import 'package:bansospedulimobile/laporan/core/laporan_list.dart';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/laporan/core/bansos_list.dart';
import 'package:bansospedulimobile/user/models/user.dart';

class BansosReportPage extends StatefulWidget {
  final User user;
  final bool justReported;
  final bool fromHomePage;
  const BansosReportPage(
      {Key? key,
      this.justReported = false,
      this.fromHomePage = false,
      required this.user})
      : super(key: key);

  @override
  _BansosReportPageState createState() => _BansosReportPageState();
}

class _BansosReportPageState extends State<BansosReportPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.justReported) {
      WidgetsBinding.instance!.addPostFrameCallback((_) => {
            ScaffoldMessenger.of(_scaffoldKey.currentContext!).showSnackBar(
                SnackBar(
                    elevation: 5,
                    backgroundColor: Colors.grey[300],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    duration: Duration(seconds: 2),
                    content: const Text(
                      "Your Report Has Been Submitted",
                      style: TextStyle(color: Colors.blue),
                    )))
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: InkWell(
          borderRadius: BorderRadius.circular(50),
          child: Icon(
            Icons.arrow_back,
            color: Theme.of(context).primaryColor,
          ),
          onTap: () {
            if (widget.fromHomePage) {
              Navigator.pop(context);
            } else {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => MyHomePage(user: widget.user)));
            }
          },
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          "BansosPeduli",
          style: TextStyle(
              fontSize: 20,
              fontStyle: FontStyle.italic,
              color: Theme.of(context).primaryColor),
        ),
      ),
      body: Center(
          child: Column(
        children: [
          Container(
            height: 80,
            width: size.width,
            child: Stack(
              children: [
                Positioned(
                    top: 33,
                    left: size.width * 0.27,
                    child: Text(
                      "Your",
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.blue[400],
                          fontWeight: FontWeight.bold),
                    )),
                Positioned(
                    left: size.width * 0.44,
                    child: AnimatedTextKit(repeatForever: true, animatedTexts: [
                      RotateAnimatedText(" Bansos",
                          textStyle:
                              TextStyle(fontSize: 30, color: Colors.blue)),
                      RotateAnimatedText(" Property",
                          textStyle:
                              TextStyle(fontSize: 30, color: Colors.blue))
                    ]))
              ],
            ),
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).primaryColor),
              onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BansosList(
                                  user: widget.user,
                                )))
                  },
              child: Text("Report Your Bansos")),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).primaryColor),
              onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LaporanList(
                                  user: widget.user,
                                )))
                  },
              child: Text("View or Update Previous Reports"))
        ],
      )),
    );
  }
}
