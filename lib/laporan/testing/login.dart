import 'dart:convert';
import 'package:bansospedulimobile/laporan/bansos_report.dart';
import 'package:bansospedulimobile/laporan/core/admin_report_dashboard.dart';
import 'package:bansospedulimobile/user/userbansos.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:bansospedulimobile/distribusi/allBansos.dart';
import 'package:bansospedulimobile/distribusi/trackingAdminDashboard.dart';
import 'package:bansospedulimobile/staff/adminInfor.dart';
import 'package:bansospedulimobile/user/userpage.dart';
import 'package:bansospedulimobile/user/userhisotry.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  Future<User> webServiceLogin(String telephone, String password) async {
    var response =
        await post(Uri.parse("http://10.0.2.2:8000/user/flutter/login/"),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode({"telephone": telephone, "password": password}));

    if (response.statusCode == 200) {
      Map<String, dynamic> userData = jsonDecode(response.body);
      User user = User(
          sessionId: userData["session-id"],
          isAdmin: userData["is_admin"],
          isCitizen: userData["is_citizen"],
          type: userData["is_admin"] ? userData["type"] : null,
          isVerified: userData["is_citizen"] ? userData["is_verified"] : null,
          telephone: userData["telephone"]);

      return user;
    } else {
      return Future.error("Incorrect Login");
    }
  }

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _telephone = TextEditingController();
  final TextEditingController _password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.blue,
        title: Text("Login"),
      ),
      body: Container(
        child: Column(
          children: [
            Form(
              child: Column(
                children: [
                  TextFormField(
                    controller: _telephone,
                  ),
                  TextFormField(
                    controller: _password,
                    obscureText: true,
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        try {
                          User user = await widget.webServiceLogin(
                              _telephone.text, _password.text);

                          if (user.isCitizen) {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        BansosReportPage(user: user)));
                          } else if (user.isAdmin && user.type == 'generator') {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      DistributionDashboard(user: user)),
                            );
                          } else if (user.isAdmin && user.type == 'tracker') {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        TrackingAdminDashboard(user)));
                          } else if (user.isAdmin &&
                              user.type == 'customer-service') {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        CustomerServiceDashboard(user: user)));
                          } else if (user.isAdmin &&
                              user.type == 'verification') {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AdminInformation(user)));
                          }
                        } catch (e) {
                          _telephone.clear();
                          _password.clear();
                        }
                      },
                      child: Text("Login"))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
