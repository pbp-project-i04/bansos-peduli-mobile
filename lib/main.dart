import 'package:bansospedulimobile/splash_screen.dart';
import 'package:flutter/material.dart';
import 'laporan/bansos_report.dart';
import 'package:bansospedulimobile/laporan/testing/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Report Dashboard',
      theme: ThemeData(
        primaryColor: Colors.blueAccent[400],
      ),
      home: const Scaffold(
        body: SplashScreen(),
      ),
    );
  }
}
