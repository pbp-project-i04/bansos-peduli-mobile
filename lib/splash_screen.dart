import 'package:bansospedulimobile/beranda/myhomepage.dart';
import 'package:bansospedulimobile/staff/adminInfor.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bansospedulimobile/staff/userinfo.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future<void> fetchSharedPreferences(BuildContext context) async {
    await Future.delayed(Duration(seconds: 2));
    final prefs = await SharedPreferences.getInstance();

    bool isExist = prefs.getString('sessionId') == null ? false : true;

    if (isExist) {
      // Construct user object
      User user = User(
          sessionId: prefs.getString('sessionId')!,
          isCitizen: prefs.getBool('isCitizen')!,
          isAdmin: prefs.getBool('isAdmin')!,
          type: prefs.getString('type')!,
          telephone: prefs.getString('telephone')!,
          isVerified: prefs.getBool('isVerified')!);

      if (user.isCitizen) {
        Map<String, dynamic> user_info = await GetUserInfo(user.telephone);

        user.isVerified = user_info["isVerified"];

        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => MyHomePage(user: user)));
      } else {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => AdminInformation(user)));
      }
    } else {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => MyHomePage()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchSharedPreferences(context),
      builder: (context, snapshot) {
        return Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.pin_drop,
                  color: Colors.blue,
                  size: 60.0,
                ),
                SizedBox(height: 10),
                Text(
                  "BansosPeduli",
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 30.0,
                    letterSpacing: -0.8,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 50),
                Container(
                  width: 100,
                  child: Image.network(
                      'https://upload.wikimedia.org/wikipedia/id/thumb/c/c3/Makara_of_Fasilkom_UI.svg/1200px-Makara_of_Fasilkom_UI.svg.png'),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
