import 'package:bansospedulimobile/staff/verifuser.dart';
import "package:flutter/material.dart";
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

//fetch data from database
Future<Map<String, dynamic>> GetUserInfo(String telephone) async {
  var url =
      'https://bansos-peduli.herokuapp.com/staff/flutter-get-user-from-tele/?telephone=$telephone';
  var response = await http.get(Uri.parse(url));
  
  if (response.statusCode == 200) {
    return jsonDecode(response.body);
  } else {
    return Future.error('An error occured');
  }
}

Future<Map<String, dynamic>> getVerifUser(
    String telephone, String verification_result) async {
  String url = 'https://bansos-peduli.herokuapp.com/staff/flutter-verification-citizen/';

  try {
    final response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8'
      },
      body: jsonEncode(<String, dynamic>{
        'telephone': telephone,
        'verification_result': verification_result
      }),
    );

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return {"isSuccessful": false, "error": "Webservice is offline."};
    }
  } catch (error) {
    return {"isSuccessful": false, "error": "Webservice is offline."};
  }
}

class UserInformation extends StatefulWidget {
  String telephone;
  UserInformation(this.telephone);

  @override
  _UserInformationState createState() => _UserInformationState();
}

class _UserInformationState extends State<UserInformation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          backgroundColor: Theme.of(context).primaryColor,
          elevation: 0,
          title: SizedBox(
            child: Text('User Information'),
          ),
        ),
        body: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(10.0),
                width: double.infinity,
                child: FutureBuilder(
                    future: GetUserInfo(widget.telephone),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        return Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              UserInformationContainer(
                                  snapshot.data as Map<String, dynamic>),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  ElevatedButton(
                                    style: ButtonStyle(
                                        backgroundColor: MaterialStateProperty.all(
                                            Colors.green[800]!)),
                                    onPressed: () async {
                                      var result = await getVerifUser(
                                          widget.telephone, 'verified');
                                      Navigator.pop(context);
                                    },
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Verify User'),
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  ElevatedButton(
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(Colors.red[800]!)),
                                    onPressed: () async {
                                      var result = await getVerifUser(
                                          widget.telephone, 'unverified');
                                      Navigator.pop(context);
                                    },
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Reject User'),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ]);
                      } else {
                        return Text('Loading...');
                      }
                    }))));
  }
}

class UserInformationContainer extends StatelessWidget {
  Map<String, dynamic> user_info = {};
  UserInformationContainer(user_info) {
    this.user_info = user_info;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      //width: 0.83 * displayWidth(context),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(color: Colors.black38, blurRadius: 10.0, offset: Offset(2, 2))
      ], borderRadius: BorderRadius.circular(10)),

      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
                color: Theme.of(context).primaryColor),
            width: double.infinity,
            child: Text(
              user_info["phone_number"],
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                letterSpacing: -0.5,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
                color: Colors.white),
            child: Column(
              children: [
                Column(
                  children: [
                    const Text(
                      'NAME',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            color: Colors.grey),
                        width: double.infinity,
                        padding: EdgeInsets.all(7),
                        margin: EdgeInsets.all(3),
                        child: Text(user_info["name"],
                            textAlign: TextAlign.center)),
                    const Text(
                      'NIK',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            color: Colors.grey),
                        width: double.infinity,
                        padding: EdgeInsets.all(7),
                        margin: EdgeInsets.all(3),
                        child: Text(user_info["nik"],
                            textAlign: TextAlign.center)),
                    const Text(
                      'SKTM',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                          color: Colors.grey),
                      width: double.infinity,
                      padding: EdgeInsets.all(7),
                      margin: EdgeInsets.all(3),
                      child: (Text(user_info["sktm"],
                          textAlign: TextAlign.center)),
                    ),
                    const Text(
                      'BUKU TABUNGAN',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                            color: Colors.grey),
                        width: double.infinity,
                        padding: EdgeInsets.all(7),
                        margin: EdgeInsets.all(3),
                        child: Text(user_info["bukutabungan"],
                            textAlign: TextAlign.center)),
                    const Text(
                      'LOCATION',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                          color: Colors.grey),
                      width: double.infinity,
                      padding: EdgeInsets.all(7),
                      margin: EdgeInsets.all(3),
                      child: (Text(user_info["location"],
                          textAlign: TextAlign.center)),
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
