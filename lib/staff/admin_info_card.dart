import 'package:flutter/material.dart';

class admin_info_card extends StatelessWidget {
  admin_info_card({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      width: 400,
      height: 700,
      padding: new EdgeInsets.all(10.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        // color: Colors.blue,
        elevation: 10,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              // leading: Icon(Icons.album, size: 60),
              title: Text(
                'My Profile',
                style: TextStyle(fontSize: 30.0),
                textAlign: TextAlign.center,
              ),
              tileColor: Colors.blue,
              // subtitle: Text(
              //     'Admin',
              //     style: TextStyle(fontSize: 18.0)
              // ),
            ),
            const ListTile(
                title: Text(
              'Admin',
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
              textAlign: TextAlign.center,
            )),
            const ListTile(
                title: Text(
              'Mughni Wibisono',
              style: TextStyle(fontSize: 18.0),
              textAlign: TextAlign.center,
            )),
            const ListTile(
                title: Text(
              'Registered Phone Number: 082312334721',
              style: TextStyle(fontSize: 18.0),
              textAlign: TextAlign.center,
            )),

            // ButtonBar(
            //   children: <Widget>[
            //     RaisedButton(
            //       child: const Text('Play'),
            //       onPressed: () {/* ... */},
            //     ),
            //     RaisedButton(
            //       child: const Text('Pause'),
            //       onPressed: () {/* ... */},
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
    ));
  }
}
