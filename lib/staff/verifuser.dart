

import 'package:bansospedulimobile/distribusi/adminBasedDrawer.dart';
import 'package:bansospedulimobile/staff/userinfo.dart';
import "package:flutter/material.dart";
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

//fetch data from database
Future<List<dynamic>> GetVerifUser(User user) async {
  var url =
      'https://bansos-peduli.herokuapp.com/staff/flutter-get-tracking-responsibilities/?session_id=${user.sessionId}';
  var response = await http.get(Uri.parse(url));
  
  if (response.statusCode == 200) {
    return jsonDecode(response.body);
  } else {
    return Future.error('An error occured');
  }
}

class VerifDash extends StatefulWidget {
  User user;
  VerifDash(this.user);

  @override
  _VerifDashState createState() => _VerifDashState();
}

class _VerifDashState extends State<VerifDash> {
  List<dynamic> listOfUsers = [];
  Future<void> InitializeData() async {
    listOfUsers = await GetVerifUser(widget.user);
  }

  void passableSetState() {
    if(mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          elevation: 0,
          centerTitle: true,
          backgroundColor: Colors.white,
          foregroundColor: Theme.of(context).primaryColor,
          title: SizedBox(
            child: Text('Verification Dashboard'),
          ),
        ),
        drawer: AdminBasedDrawer(widget.user),
        body: SingleChildScrollView(
            child: Container(
                width: double.infinity,
                child: FutureBuilder(
                  future: InitializeData(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            VerificationContainer(
                                'Unverified User', listOfUsers, passableSetState),
                            VerificationContainer('Verified User', listOfUsers, passableSetState),
                          ]);
                    } else {
                      return Text('Loading...');
                    }
                  },
                ))));
  }
}

class VerificationContainer extends StatelessWidget {
  late String title;
  late List<dynamic> user_data;
  late Color selectedColor;
  late Function passableSetState;

  VerificationContainer(title, user_data, passableSetState) {
    this.title = title;
    this.user_data = user_data;
    this.passableSetState = passableSetState;

    if (title == 'Unverified User') {
      selectedColor = Colors.red[900]!;
    } else {
      selectedColor = Colors.green[900]!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: 0.83 * displayWidth(context),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(color: Colors.black38, blurRadius: 10.0, offset: Offset(2, 2))
      ], borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
                color: Theme.of(context).primaryColor),
            width: double.infinity,
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                letterSpacing: -0.5,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
                color: Colors.white),
            child: Column(
              children: [
                for (int i = 0; i < user_data.length; i++)
                  if ((title == 'Verified User' &&
                          user_data[i]["fields"]["is_verified"]) ||
                      (title == 'Unverified User' &&
                          !user_data[i]["fields"]["is_verified"]))
                    Column(
                      children: [
                        TextButton(
                          style: ButtonStyle(
                              minimumSize: MaterialStateProperty.all(
                                  Size(double.infinity, 50)),
                              backgroundColor:
                                  MaterialStateProperty.all(selectedColor),
                              foregroundColor:
                                  MaterialStateProperty.all(Colors.white)),
                          onPressed: () async {
                            await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        UserInformation(user_data[i]["pk"])));
                            passableSetState();
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Phone Number: ${user_data[i]["pk"]}'),
                              Text('Name: ${user_data[i]["fields"]["name"]}'),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        )
                      ],
                    )
              ],
            ),
          )
        ],
      ),
    );
  }
}
