import 'dart:convert';

import 'package:bansospedulimobile/distribusi/adminBasedDrawer.dart';
import 'package:bansospedulimobile/distribusi/testing/logout.dart';
import "package:flutter/material.dart";
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:http/http.dart' as http;

//fetch data from database
Future<Map<String, dynamic>> GetAdminInfo(User user) async {
  var url =
      'https://bansos-peduli.herokuapp.com/staff/flutter-get-admin-info/?session_id=${user.sessionId}';
  var response = await http.get(Uri.parse(url));
  if (response.statusCode == 200) {
    return jsonDecode(response.body);
  } else {
    return Future.error('An error occured');
  }
}

class AdminInformation extends StatefulWidget {
  User user;
  AdminInformation(this.user);

  @override
  _AdminInformationState createState() => _AdminInformationState();
}

class _AdminInformationState extends State<AdminInformation> {
  void initState() {
    GetAdminInfo(widget.user);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        backgroundColor: Colors.white,
        foregroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        title: SizedBox(
          child: Text('Admin Information'),
        ),
      ),
      drawer: AdminBasedDrawer(widget.user),
      body: SingleChildScrollView(
          child: FutureBuilder(
        future: GetAdminInfo(widget.user),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Container(
                width: double.infinity,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AdminInformationContainer(
                          'My Profile', snapshot.data as Map<String, dynamic>),
                      ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.red)),
                        onPressed: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      LogoutPage(widget.user)));
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Log Out'),
                          ],
                        ),
                      ),
                    ]));
          } else {
            return Text('Loading...');
          }
        },
      )),
    );
  }
}

class AdminInformationContainer extends StatelessWidget {
  late String title;
  late Map<String, dynamic> user_map;
  AdminInformationContainer(String title, Map<String, dynamic> user_map) {
    title == 'My Profile';
    this.title = title;
    this.user_map = user_map;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: 0.83 * displayWidth(context),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(color: Colors.black38, blurRadius: 10.0, offset: Offset(2, 2))
      ], borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                  // bottomLeft: Radius.circular(10),
                  // bottomRight: Radius.circular(10),
                ),
                color: Theme.of(context).primaryColor),
            width: double.infinity,
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                letterSpacing: -0.8,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
                color: Colors.white),
            child: Column(
              children: [
                Column(
                  children: [
                    Container(
                      height: 150,
                      width: 150,
                      child: Image.network(
                          'https://images-ext-2.discordapp.net/external/qQ860k8nw0RuE6wb4fA-0GrjnXRlYaY2gGzLk7s0mFg/https/cdn0.iconfinder.com/data/icons/communication-456/24/account_profile_user_contact_person_avatar_placeholder-512.png?width=375&height=375'),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "Admin",
                        style: TextStyle(
                          fontSize: 17,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),

                    ),
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          )),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            ' ${user_map["name"]}',
                            style: TextStyle(fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          )),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '${user_map["admin_type"]}',
                            style: TextStyle(
                              color: Colors.grey[800],
                              
                            )
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                          color: Colors.grey),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Phone Number: ${user_map["phone_number"]}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
