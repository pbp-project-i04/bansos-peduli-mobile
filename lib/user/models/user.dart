import 'package:http/http.dart';
import 'dart:async';

class User {
  String sessionId;
  bool isCitizen;
  bool isAdmin;
  bool? isVerified;
  String? type;
  String telephone;

  User(
      {required this.sessionId,
      required this.isCitizen,
      required this.isAdmin,
      this.type,
      this.isVerified,
      required this.telephone});
}
