import 'dart:convert';

import 'package:bansospedulimobile/user/userbansos.dart';
import 'package:bansospedulimobile/user/userhisotry.dart';
import "package:flutter/material.dart";
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:bansospedulimobile/user/editprofile.dart';

Future<Map<String, dynamic>> GetAdminInfo(User user) async {
  var url =
      'https://bansos-peduli.herokuapp.com/user/flutter-get-user-info/?session_id=${user.sessionId}';
  var response = await http.get(Uri.parse(url));
  
  if (response.statusCode == 200) {
    return jsonDecode(response.body);
  } else {
    return Future.error('An error occured');
  }
}


class UserInformation extends StatefulWidget {
  User user;
  UserInformation(this.user);

  @override
  _UserInformationState createState() => _UserInformationState();
}

class _UserInformationState extends State<UserInformation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        backgroundColor: Colors.white,
        foregroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        title: SizedBox(
          child: Text('User Information'),
        ),
      ),
      body: SingleChildScrollView(
          child: FutureBuilder(
        future: GetAdminInfo(widget.user),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Container(
                width: double.infinity,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AdminInformationContainer(
                          'My Profile', snapshot.data as Map<String, dynamic>),
                      ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.red)),
                        onPressed: () async {
                          await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      UpdateProfile(widget.user)));
                          setState(() {});
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Edit Profile'),
                          ],
                        ),
                      ),
                    ]));
          } else {
            return Text('Loading...');
          }
        },
      )),
      bottomNavigationBar:
          BottomNavigationBar(
          currentIndex: 0,
          items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: IconButton(
            onPressed: () {
              
            },
            icon: Icon(Icons.person),
          ),
          label: 'Profile',
        ),
        BottomNavigationBarItem(
          icon: IconButton(
            icon: Icon(Icons.check),
            onPressed: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder:(context) => UserBansosInfo(widget.user))
              );
            }
          ),
          label: 'Bansos Info',
          
        ),
        BottomNavigationBarItem(
          icon: IconButton(
            icon: Icon(Icons.history),
            onPressed: () {
              Navigator.pushReplacement(
                context, 
                MaterialPageRoute(builder: (context) => UserBansos(widget.user))
              );
            }
          ),
          label: 'Bansos History', 
        ),
      ]),
    );
  }
}

class AdminInformationContainer extends StatelessWidget {
  late String title;
  late Map<String, dynamic> user_map;
  AdminInformationContainer(String title, Map<String, dynamic> user_map) {
    
    this.title = title;
    this.user_map = user_map;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: 0.83 * displayWidth(context),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(color: Colors.black38, blurRadius: 10.0, offset: Offset(2, 2))
      ], borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
                color: Theme.of(context).primaryColor),
            width: double.infinity,
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
                color: Colors.white),
            child: Column(
              children: [
                Column(
                  children: [
                    TextButton(
                      style: ButtonStyle(
                          minimumSize: MaterialStateProperty.all(
                              Size(double.infinity, 10)),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey),
                          foregroundColor:
                              MaterialStateProperty.all(Colors.black)),
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Name: ${user_map["name"]}'),
                        ],
                      ),
                    ),
                    TextButton(
                      style: ButtonStyle(
                          minimumSize: MaterialStateProperty.all(
                              Size(double.infinity, 10)),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey),
                          foregroundColor:
                              MaterialStateProperty.all(Colors.black)),
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('NIK: ${user_map["NIK"]}'),
                        ],
                      ),
                    ),
                    TextButton(
                      style: ButtonStyle(
                          minimumSize: MaterialStateProperty.all(
                              Size(double.infinity, 10)),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey),
                          foregroundColor:
                              MaterialStateProperty.all(Colors.black)),
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('SKTM: ${user_map["SKTM"]}'),
                        ],
                      ),
                    ),
                    TextButton(
                      style: ButtonStyle(
                          minimumSize: MaterialStateProperty.all(
                              Size(double.infinity, 10)),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey),
                          foregroundColor:
                              MaterialStateProperty.all(Colors.black)),
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Buku Tabungan: ${user_map["BukuTabungan"]}'),
                        ],
                      ),
                    ),
                    TextButton(
                      style: ButtonStyle(
                          minimumSize: MaterialStateProperty.all(
                              Size(double.infinity, 10)),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey),
                          foregroundColor:
                              MaterialStateProperty.all(Colors.black)),
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Area: ${user_map["Area"]}'),
                        ],
                      ),
                    ),
                    TextButton(
                      style: ButtonStyle(
                          minimumSize: MaterialStateProperty.all(
                              Size(double.infinity, 10)),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey),
                          foregroundColor:
                              MaterialStateProperty.all(Colors.black)),
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              'Verification Status: ${user_map["verification"]}'),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
