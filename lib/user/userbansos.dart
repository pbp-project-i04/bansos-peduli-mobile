import 'dart:convert';
import 'package:bansospedulimobile/user/userhisotry.dart';
import 'package:bansospedulimobile/user/userpage.dart';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/distribusi/registerArea.dart';
import 'package:bansospedulimobile/distribusi/adminBasedDrawer.dart';
import 'package:bansospedulimobile/user/detailbansos.dart';
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:bansospedulimobile/distribusi/generateBansos.dart';
import 'package:http/http.dart' as http;
import 'package:bansospedulimobile/distribusi/trackingAdminDashboard.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'dart:async';

Future<Map<String, dynamic>> fetchGroups(User user) async {
  String url =
      'https://bansos-peduli.herokuapp.com/user/flutter-user-info-bansos/?session_id=${user.sessionId}';

  try {
    Map<String, String> headers = {
      'Content-Type': 'application/json; charset=UTF-8',
    };
    Map<String, dynamic> body = {'sessionId': user.sessionId};

    final response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(body),
    );

    List<dynamic> extractedData = jsonDecode(response.body);

    // await Future.delayed(Duration(seconds: 10));
    if (response.statusCode == 200) {
      return {"isSuccessful": true, "data": extractedData, "error": null};
    } else {
      return {
        "isSuccessful": false,
        "data": extractedData,
        "error": "An error has occurred"
      };
    }
  } catch (error) {
    return {
      "isSuccessful": false,
      "data": [],
      "error": "Our web service is down."
    };
  }
}

class UserBansosInfo extends StatefulWidget {
  final User user;

  UserBansosInfo(this.user);

  @override
  State<UserBansosInfo> createState() => _UserBansosInfoState();
}

class _UserBansosInfoState extends State<UserBansosInfo> {
  int _counter = 0;
  List<dynamic> _all_bansos = [];
  Map<String, dynamic> response = {};
  String? current_error;
  bool inRefresh = false;
  late Timer _timer;

  @override
  void initState() {
    super.initState();

    _timer = Timer.periodic(Duration(seconds: 8), (timer) async {
      await _intializeData();
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Future<void> _intializeData() async {
    response = await fetchGroups(widget.user);
    inRefresh = false;

    if (response["isSuccessful"]) {
      current_error = null;
      _all_bansos = response["data"];
    } else {
      current_error = response["error"];
    }
  }

  Future<void> _refresh() async {
    setState(() {
      inRefresh = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          backgroundColor: Colors.white,
          foregroundColor: Theme.of(context).primaryColor,
          elevation: 0,
          title: SizedBox(
            child: Text('User Dashboard'),
          ),
        ),
        bottomNavigationBar:
          BottomNavigationBar(
          currentIndex: 1,
          items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: IconButton(
            onPressed: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder:(context) =>  UserInformation(widget.user))
              );
            },
            icon: Icon(Icons.person),
          ),
          label: 'Profile',
        ),
        BottomNavigationBarItem(
          icon: IconButton(
            icon: Icon(Icons.check),
            onPressed: () {}
          ),
          label: 'Bansos Info',
          
        ),
        BottomNavigationBarItem(
          icon: IconButton(
            icon: Icon(Icons.history),
            onPressed: () {
              Navigator.pushReplacement(
                context, 
                MaterialPageRoute(builder: (context) => UserBansos(widget.user))
              );
            }
          ),
          label: 'Bansos History', 
        ),
      ]),
        body: RefreshIndicator(
            onRefresh: _refresh,
            child: Column(
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      FutureBuilder(
                          future: _intializeData(),
                          builder: (context, snapshot) {
                            if (_all_bansos.isNotEmpty ||
                                snapshot.connectionState ==
                                    ConnectionState.done) {
                              if (current_error != null) {
                                return Container(
                                    width: 0.75 * displayWidth(context),
                                    child: Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        margin: EdgeInsets.only(
                                            top: displayHeight(context) * 0.1),
                                        elevation: 0,
                                        child: Padding(
                                          padding: EdgeInsets.all(20),
                                          child: Column(children: [
                                            Icon(
                                              Icons
                                                  .signal_wifi_statusbar_connected_no_internet_4_rounded,
                                              size: 200,
                                              color: Colors.blue,
                                            ),
                                            Text(
                                              "Sorry :(",
                                              style: TextStyle(
                                                color: Colors.blue,
                                                fontSize: 30,
                                                fontWeight: FontWeight.bold,
                                                letterSpacing: -0.8,
                                              ),
                                            ),
                                            SizedBox(height: 10),
                                            Text(
                                              current_error!,
                                              style: TextStyle(
                                                color: Colors.grey.shade700,
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                letterSpacing: -0.8,
                                              ),
                                            ),
                                            !inRefresh
                                                ? TextButton(
                                                    child: Text("Try Again."),
                                                    onPressed: () {
                                                      setState(() {});
                                                    },
                                                    style: ButtonStyle(
                                                      minimumSize:
                                                          MaterialStateProperty
                                                              .all(Size(
                                                                  double
                                                                      .infinity,
                                                                  30)),
                                                      backgroundColor:
                                                          MaterialStateProperty
                                                              .all(Colors.blue),
                                                      foregroundColor:
                                                          MaterialStateProperty
                                                              .all(
                                                                  Colors.white),
                                                    ),
                                                  )
                                                : TextButton(
                                                    child: Container(
                                                      height: 20,
                                                      width: 20,
                                                      child:
                                                          CircularProgressIndicator(
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                    onPressed: () {},
                                                    style: ButtonStyle(
                                                      minimumSize:
                                                          MaterialStateProperty
                                                              .all(Size(
                                                                  double
                                                                      .infinity,
                                                                  30)),
                                                      backgroundColor:
                                                          MaterialStateProperty
                                                              .all(Colors.blue
                                                                  .shade300),
                                                      foregroundColor:
                                                          MaterialStateProperty
                                                              .all(
                                                                  Colors.white),
                                                    ),
                                                  ),
                                          ]),
                                        )));
                              }

                              if (_all_bansos.length > 0) {
                                return Container(
                                  margin: EdgeInsets.fromLTRB(
                                      0.0 * displayWidth(context), 0, 0.065, 0),
                                  child: Column(
                                    children: [
                                      if (inRefresh) LinearProgressIndicator(color: Theme.of(context).primaryColor,),
                                      for (int i = 0;
                                          i < _all_bansos.length;
                                          i++)
                                        BansosCards(
                                          _all_bansos[i]["fields"]["status"],
                                          _all_bansos[i]["pk"],
                                          _all_bansos[i]["fields"]
                                              ["destination_kelurahan"],
                                          _all_bansos[i]["fields"]
                                              ["destination_kecamatan"],
                                          _all_bansos[i]["fields"]
                                              ["destination_kabupaten_kota"],
                                          _all_bansos[i]["fields"]
                                              ["receiving_provinsi"],
                                          _all_bansos[i]["fields"]
                                              ["total_receiver"],
                                          _all_bansos[i]["fields"]
                                              ["bentuk_bantuan"],
                                          _all_bansos[i]["fields"]
                                              ["satuan_bantuan"],
                                          _all_bansos[i]["fields"]["timestamp"],
                                        ),
                                    ],
                                  ),
                                );
                              } else {
                                return Container(
                                  margin: EdgeInsets.only(
                                      top: 0.1 * displayHeight(context)),
                                  child: Column(
                                    children: [
                                      Icon(
                                        Icons.warning_rounded,
                                        size: 100,
                                      ),
                                      Text(
                                          "It seems like all your bansos has arrived."),
                                    ],
                                  ),
                                );
                              }
                            } else if (snapshot.hasError) {
                              return Center(
                                child: Column(children: [
                                  Icon(Icons.warning),
                                  Text("We are sorry. An error has occurred."),
                                ]),
                              );
                            } else {
                              return LinearProgressIndicator(color: Theme.of(context).primaryColor);
                            }
                          }),
                    ],
                  ),
                ),
              ],
            ))
        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}

class BansosCards extends StatelessWidget {
  Color? finalStateColor;
  String? uuid;
  String? destinationKelurahan;
  String? destinationKecamatan;
  String? destinationKabKota;
  String? destinationProvinsi;
  int? total_receiver;
  Map<dynamic, dynamic> bentuk_bantuan;
  Map<dynamic, dynamic> satuan_bantuan;
  String? time_stamp;

  BansosCards(
      String status,
      this.uuid,
      this.destinationKelurahan,
      this.destinationKecamatan,
      this.destinationKabKota,
      this.destinationProvinsi,
      this.total_receiver,
      this.bentuk_bantuan,
      this.satuan_bantuan,
      this.time_stamp) {
    finalStateColor = Colors.red[900];
  }

  @override
  Widget build(BuildContext context) {
    List<String> tipe_bantuan = [];
    bentuk_bantuan.forEach((key, value) {
      tipe_bantuan.add(key);
    });
    return Container(
        margin: EdgeInsets.only(top: 20),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 15.0,
              offset: Offset(2.0, 4.0),
            )
          ],
        ),
        child: Column(
          children: [
            Container(
              width: 0.83 * displayWidth(context),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
                color: finalStateColor,
              ),
              child: Text(
                'Bansos to $destinationProvinsi',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
              padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Text(uuid!),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: FlatButton(
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    "Bansos Details",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UserDetailsBansos("$uuid")),
                    );
                  }),
            ),
          ],
        ));
  }
}
