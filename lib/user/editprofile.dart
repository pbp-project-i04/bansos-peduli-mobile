import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:http/http.dart' as http;
import 'package:bansospedulimobile/user/models/user.dart';

Future<Map<String, dynamic>> sendNewUser(
    String new_SKTM, String new_buku, User user) async {
  final url = 'https://bansos-peduli.herokuapp.com/user/flutter-update-user/';

  try {
    final response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'sktm': new_SKTM,
        'buku': new_buku,
        'session_id': user.sessionId,
      }),
    );
    print('masuk');

    Map<String, dynamic> result = jsonDecode(response.body);

    if (response.statusCode == 200) {
      return result;
    } else {
      return <String, dynamic>{'error': 'Web service is offline'};
    }
  } catch (error) {
    return {'isSuccessful': false, 'error': ''};
  }
}

class UpdateProfile extends StatefulWidget {
  User user;
  UpdateProfile(this.user);
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController sktm = TextEditingController();
  TextEditingController buku = TextEditingController();
  Map<String, dynamic>? fetchedResult;
  bool? isSuccessful;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
          padding: EdgeInsets.all(10.0),
          width: 0.85 * displayWidth(context),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 9.0,
                offset: Offset(4.0, 4.0),
              )
            ],
          ),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                    controller: sktm,
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.phone),
                      hintText: 'Enter your SKTM',
                      labelText: 'SKTM',
                    ),
                    validator: (String? value) {
                      if (value!.isEmpty) {
                        return "SKTM can't be empty";
                      }

                      return null;
                    }),
                TextFormField(
                    controller: buku,
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.phone),
                      hintText: 'Enter your Buku Tabungan',
                      labelText: 'Buku Tabungan',
                    ),
                    validator: (String? value) {
                      if (value!.isEmpty) {
                        return "Buku Tabungan can't be empty";
                      }

                      return null;
                    }),
                TextButton(
                  child: Text(
                    "Save",
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(Colors.blueAccent[700]),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      fetchedResult =
                          await sendNewUser(sktm.text, buku.text, widget.user);
                      isSuccessful = fetchedResult!["isSuccessful"];
                      print(isSuccessful);
                      if (isSuccessful!) {
                        Navigator.pop(context);
                      } else {
                        setState(() {
                          final snackBar = SnackBar(
                            content: Text(fetchedResult!["error"]),
                            behavior: SnackBarBehavior.floating,
                            backgroundColor: Colors.blue,
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        });
                      }
                    } else {}
                  },
                ),
                SizedBox(width: 5),
                TextButton(
                  child: Text(
                    "Cancel",
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.red[700]),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          )),
    );
  }
}
