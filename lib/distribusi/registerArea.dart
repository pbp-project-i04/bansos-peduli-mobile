import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/distribusi/adminBasedDrawer.dart';
import 'package:bansospedulimobile/distribusi/allBansos.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:http/http.dart' as http;

Future<Map<String, dynamic>> sendInput(
    String area_code, String area_name) async {
  String url =
      'https://bansos-peduli.herokuapp.com/distribusi/mob-register-area/';

  try {
    final response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8'
      },
      body: jsonEncode(<String, String>{
        'area-code': area_code,
        'area-name': area_name,
      }),
    );
    bool isSuccessful = jsonDecode(response.body)["isSuccessful"];

    if (response.statusCode == 200) {
      return {'isSuccessful': isSuccessful, 'error': null};
    } else {
      return {'isSuccessful': false, 'error': 'Something went wrong.'};
    }
  } catch (error) {
    return {
      'isSuccessful': false,
      'error': 'We are sorry. Our web service is down.'
    };
  }
}

class RegisterArea extends StatefulWidget {
  User user;
  RegisterArea(this.user);

  @override
  _RegisterAreaState createState() => _RegisterAreaState();
}

class _RegisterAreaState extends State<RegisterArea> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _provinceCodeController = TextEditingController();
  TextEditingController _provinceNameController = TextEditingController();

  String provinceCode = '';
  String provinceName = '';
  bool flagDuplicate = false;
  bool hideMessage = true;
  bool showCircularProgress = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
        title: Row(
          children: const [
            Icon(
              Icons.pin_drop,
            ),
            Text('BansosPeduli'),
          ],
        ),
      ),
      body: Builder(
        builder: (context) => Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(20.0, 25.0, 20.0, 0),
                    child: Column(
                      children: [
                        Container(
                          color: Theme.of(context).primaryColor,
                          padding: EdgeInsets.all(5),
                          child: Text(
                            'Register Area',
                            style: TextStyle(
                              fontSize: 35,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(3.0, 8.0, 8.0, 3.0),
                          child: TextFormField(
                            controller: _provinceCodeController,
                            decoration: InputDecoration(
                              fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                              filled: true,
                              hintText: "contoh: JB",
                              labelText: "Province Code",
                              prefixIcon:
                                  Icon(Icons.confirmation_number_rounded),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Province Code should not be empty.';
                              }
                              return null;
                            },
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(3.0, 8.0, 8.0, 3.0),
                          child: TextFormField(
                            controller: _provinceNameController,
                            decoration: InputDecoration(
                              fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                              filled: true,
                              hintText: "contoh: Jawa Barat",
                              labelText: "Province Name",
                              prefixIcon: Icon(Icons.location_city_rounded),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Province name should not be empty.';
                              }
                              return null;
                            },
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        if (showCircularProgress)
                          Container(
                              height: 38,
                              width: 115,
                              margin: EdgeInsets.only(top: 5),
                              child: TextButton(
                                  child: Container(
                                    height: 20,
                                    width: 20,
                                    child: CircularProgressIndicator(
                                      color: Colors.white,
                                      strokeWidth: 3,
                                    ),
                                  ),
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        Colors.blueAccent[200]),
                                  ),
                                  onPressed: () {}))
                        else
                          ElevatedButton(
                            child: const Text(
                              'Register',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                provinceCode = _provinceCodeController.text;
                                provinceName = _provinceNameController.text;

                                setState(() {
                                  showCircularProgress = true;
                                });

                                sendInput(provinceCode, provinceName)
                                    .then((result) {
                                  setState(() {
                                    showCircularProgress = false;

                                    if (result["error"] == null) {
                                      flagDuplicate = !result["isSuccessful"];
                                      hideMessage = false;
                                    } else {
                                      final snackBar = SnackBar(
                                        content: Text(result["error"]),
                                        behavior: SnackBarBehavior.floating,
                                        backgroundColor: Theme.of(context).primaryColor,
                                      );
                                      Scaffold.of(context)
                                          .showSnackBar(snackBar);
                                    }
                                  });
                                });
                              } else {
                                setState(() {
                                  hideMessage = true;
                                });
                              }
                            },
                            style: ButtonStyle(
                              elevation: MaterialStateProperty.all<double>(0),
                              backgroundColor: MaterialStateProperty.all(Theme.of(context).primaryColor),
                            ),
                          ),
                        const SizedBox(
                          height: 10,
                        ),
                        if (!flagDuplicate && !hideMessage)
                          Container(
                            padding: EdgeInsets.all(8),
                            color: Colors.blue[100],
                            child: Column(children: [
                              const Text("Notes on Previous Entry:",
                                  style: TextStyle(
                                    fontSize: 13,
                                  )),
                              Text(
                                "[Accepted] Province " +
                                    provinceName +
                                    " with code " +
                                    provinceCode +
                                    " was registed successfully.",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 13,
                                ),
                              ),
                            ]),
                          )
                        else if (!hideMessage)
                          Container(
                            padding: EdgeInsets.all(8),
                            color: Colors.red[100],
                            child: Column(children: [
                              const Text("Notes on Previous Entry:",
                                  style: TextStyle(
                                    fontSize: 13,
                                  )),
                              Text(
                                "[Rejected] Province " +
                                    provinceName +
                                    " has been registered.",
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  fontSize: 13,
                                ),
                              ),
                            ]),
                          ),
                        const SizedBox(
                          height: 20,
                        ),
                        Image.network(
                          'https://www.drupal.org/files/project-images/reg_confirm_email_with_button_0.png',
                          height: 200,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
