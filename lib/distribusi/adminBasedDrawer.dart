import 'package:bansospedulimobile/distribusi/trackingAdminDashboard.dart';
import 'package:bansospedulimobile/laporan/bansos_report.dart';
import 'package:bansospedulimobile/laporan/core/admin_report_dashboard.dart';
import 'package:bansospedulimobile/registration/admin_registration.dart';
import 'package:bansospedulimobile/staff/adminInfor.dart';
import 'package:bansospedulimobile/staff/verifuser.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:bansospedulimobile/distribusi/allBansos.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:bansospedulimobile/distribusi/testing/logout.dart';

class AdminBasedDrawer extends StatelessWidget {
  User admin;

  AdminBasedDrawer(this.admin);

  @override
  Widget build(BuildContext context) {
    String dashboardTitle;

    if (admin.type == 'tracker') {
      dashboardTitle = 'Tracking Admin Dashboard';
    } else if (admin.type == 'generator') {
      dashboardTitle = 'Generator Admin Dashboard';
    } else if (admin.type == 'verification') {
      dashboardTitle = 'Verification Dashboard';
    } else {
      dashboardTitle = 'Customer-Service Dashboard';
    }

    return Drawer(
      child: ListView(children: [
        ListTile(
          title: Align(
            child: Text(
              'BansosPeduli',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontFamily: 'Roboto',
                fontSize: 30,
                fontWeight: FontWeight.w900,
              ),
            ),
            alignment: Alignment(-1.0, 0),
          ),
        ),
        const Divider(
          color: Colors.grey,
        ),
        ListTile(
          leading: Icon(Icons.account_circle),
          title: const Align(
            child: Text('My Profile'),
            alignment: Alignment.centerLeft,
          ),
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => AdminInformation(admin)));
          },
        ),
        ListTile(
          leading: Icon(Icons.dashboard),
          title: Align(
            child: Text(dashboardTitle),
            alignment: Alignment.centerLeft,
          ),
          onTap: () {
            if (admin.type == 'tracker')
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => TrackingAdminDashboard(admin)),
              );
            else if (admin.type == 'generator')
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => DistributionDashboard(user: admin)),
              );
            else if (admin.type == 'verification')
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => VerifDash(admin)),
              );
            else if (admin.type == 'customer-service') {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          CustomerServiceDashboard(user: admin)));
            }
          },
        ),
        if (admin.type == 'generator')
          ListTile(
            leading: Icon(Icons.app_registration_rounded),
            title: const Align(
              child: Text('Register Admin'),
              alignment: Alignment.centerLeft,
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AdminRegistration()));
            },
          ),
        ListTile(
          leading: Icon(Icons.logout),
          title: const Align(
            child: Text('Logout'),
            alignment: Alignment.centerLeft,
          ),
          onTap: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => LogoutPage(admin)),
            );
          },
        ),
      ]),
    );
  }
}
