import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/distribusi/adminBasedDrawer.dart';
import 'package:bansospedulimobile/distribusi/allBansos.dart';
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:bansospedulimobile/distribusi/bansosDetail.dart';

Future<Map<String, dynamic>> fetchArea() async {
  String url =
      'https://bansos-peduli.herokuapp.com/distribusi/get-area-information/';

  try {
    final response = await http.get(Uri.parse(url));
    return jsonDecode(response.body);
  } catch (error) {
    return {};
  }
}

Future<Map<String, dynamic>> sendBansos(
    String originUrbanVillage,
    String originSubDistrict,
    String originCity,
    String originProvinsi,
    String targetUrbanVillage,
    String targetSubDistrict,
    String targetCity,
    String targetProvinsi,
    Map bansosValue,
    Map bansosUnit,
    String trackingAdmin) async {
  String strBansosValue = bansosValue.toString();
  
  String url =
      'https://bansos-peduli.herokuapp.com/distribusi/mob-register-bansos/';

  if (bansosValue.length <= 0) {
    return {"isSuccessful": false, "error": "Bansos type should not be empty."};
  }

  try {
    final response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8'
      },
      body: jsonEncode(<String, dynamic>{
        'input_kelurahan': originUrbanVillage,
        'input_kecamatan': originSubDistrict,
        'input_kabupaten_kota': originCity,
        'input_provinsi': originProvinsi,
        'input_kelurahan_target': targetUrbanVillage,
        'input_kecamatan_target': targetSubDistrict,
        'input_kabupaten_kota_target': targetCity,
        'area': targetProvinsi,
        'bansosValue': bansosValue,
        'bansosUnit': bansosUnit,
        'tracking-admin': trackingAdmin
      }),
    );

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return {"isSuccessful": false, "error": "Webservice is offline."};
    }
  } catch (error) {
    return {"isSuccessful": false, "error": "Webservice is offline."};
  }
}

Future<List<dynamic>> fetchAdmins() async {
  String url = 'https://bansos-peduli.herokuapp.com/distribusi/mob-get-admin/';

  try {
    final response = await http.get(Uri.parse(url));
    return jsonDecode(response.body);
  } catch (error) {
    return [];
  }
}

class EntryBansos extends StatefulWidget {
  User user;
  EntryBansos(this.user);

  @override
  _EntryBansosState createState() => _EntryBansosState();
}

class _EntryBansosState extends State<EntryBansos> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _originUrbanVillage = TextEditingController();
  TextEditingController _originSubDistrict = TextEditingController();
  TextEditingController _originCity = TextEditingController();

  String? _originProvinsi;

  TextEditingController _targetUrbanVillage = TextEditingController();
  TextEditingController _targetSubDistrict = TextEditingController();
  TextEditingController _targetCity = TextEditingController();
  TextEditingController _targetProvince = TextEditingController();

  String? _targetProvinsi;

  String? _trackingAdmin;

  int numbOfBansos = 0;

  Map<String, TextEditingController> bansosControllers = {};
  Map<String, TextEditingController> valueControllers = {};
  Map<String, TextEditingController> unitControllers = {};

  Map<String, dynamic> bansosValue = {};
  Map<String, dynamic> bansosUnit = {};
  Map<String, dynamic> _areas = {};

  List<dynamic> _admins = [];

  bool showCircularProgress = false;
  bool showLinearProgress = true;

  @override
  void initState() {
    initializer();
  }

  Future<void> initializer() async {
    var resultAreas = await fetchArea();
    var resultAdmins = await fetchAdmins();
    if(mounted) {
      setState(() {
        _areas = resultAreas;
        _admins = resultAdmins;
        showLinearProgress = false;
      });
    } 
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          elevation: 0,
          title: Row(
            children: const [
              Icon(
                Icons.pin_drop,
              ),
              Text('BansosPeduli'),
            ],
          ),
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(children: [
              Container(
                height: 40,
                width: displayWidth(context),
                color: Theme.of(context).primaryColor,
                child: Text(
                  "Input Data Bansos",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              if (showLinearProgress) LinearProgressIndicator(color: Theme.of(context).primaryColor),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    width: 0.83 * displayWidth(context),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 10.0,
                          offset: Offset(2.0, 2.0),
                        )
                      ],
                      color: Colors.white,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                            padding: EdgeInsets.all(15),
                            child: Text(
                              "Origin Location Information",
                              style: TextStyle(
                                fontWeight: FontWeight.w800,
                                fontSize: 18,
                                letterSpacing: -0.8,
                              ),
                            )),
                        Padding(
                          padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
                          child: TextFormField(
                              controller: _originUrbanVillage,
                              decoration: InputDecoration(
                                fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                                filled: true,
                                hintText: "contoh: Jakasetia",
                                labelText: "Origin Urban Village",
                                prefixIcon: Icon(Icons.pin_drop),
                                contentPadding: EdgeInsets.all(3),
                                border: InputBorder.none,
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Origin Urban Village should not be empty';
                                }
                              }),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          child: TextFormField(
                              controller: _originSubDistrict,
                              decoration: InputDecoration(
                                fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                                filled: true,
                                hintText: "contoh: Bekasi Selatan",
                                labelText: "Origin Sub-District",
                                prefixIcon: Icon(Icons.house_rounded),
                                contentPadding: EdgeInsets.all(3),
                                border: InputBorder.none,
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Origin Sub-district should not be empty';
                                }
                              }),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          child: TextFormField(
                              controller: _originCity,
                              decoration: InputDecoration(
                                fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                                filled: true,
                                hintText: "contoh: Kota Bekasi",
                                labelText: "Origin District/City",
                                prefixIcon: Icon(Icons.location_city_rounded),
                                contentPadding: EdgeInsets.all(3),
                                border: InputBorder.none,
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Origin District/City should not be empty';
                                }
                              }),
                        ),
                        Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 15),
                            child: Container(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              color: Color.fromRGBO(238, 238, 238, 0.93),
                              child: DropdownButtonFormField<String>(
                                decoration: InputDecoration(
                                  fillColor:
                                      Color.fromRGBO(238, 238, 238, 0.93),
                                  filled: true,
                                  labelText: "Origin Province",
                                  prefixIcon: Icon(Icons.map_rounded),
                                  contentPadding: EdgeInsets.all(3),
                                  border: InputBorder.none,
                                ),
                                value: _originProvinsi,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Origin province should not be empty';
                                  }
                                },
                                isExpanded: true,
                                icon: const Icon(Icons.arrow_drop_down_rounded),
                                elevation: 16,
                                onChanged: (String? newValue) {
                                  setState(() {
                                    _originProvinsi = newValue!;
                                  });
                                },
                                items: _areas.keys
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(_areas[value]["area_name"]),
                                  );
                                }).toList(),
                              ),
                            )),
                        Padding(
                            padding: EdgeInsets.all(15),
                            child: Text(
                              "Target Location Information",
                              style: TextStyle(
                                fontWeight: FontWeight.w800,
                                fontSize: 18,
                                letterSpacing: -0.8,
                              ),
                            )),
                        Padding(
                          padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
                          child: TextFormField(
                              controller: _targetUrbanVillage,
                              decoration: InputDecoration(
                                fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                                filled: true,
                                hintText: "contoh: Jakasetia",
                                labelText: "Target Urban Village",
                                prefixIcon: Icon(Icons.pin_drop),
                                contentPadding: EdgeInsets.all(3),
                                border: InputBorder.none,
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Target Urban Village should not be empty';
                                }
                              }),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          child: TextFormField(
                              controller: _targetSubDistrict,
                              decoration: InputDecoration(
                                fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                                filled: true,
                                hintText: "contoh: Bekasi Selatan",
                                labelText: "Target Sub-District",
                                prefixIcon: Icon(Icons.house_rounded),
                                contentPadding: EdgeInsets.all(3),
                                border: InputBorder.none,
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Target Sub-district should not be empty';
                                }
                              }),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          child: TextFormField(
                              controller: _targetCity,
                              decoration: InputDecoration(
                                fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                                filled: true,
                                hintText: "contoh: Kota Bekasi",
                                labelText: "Target District/City",
                                prefixIcon: Icon(Icons.location_city_rounded),
                                contentPadding: EdgeInsets.all(3),
                                border: InputBorder.none,
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Target District/City should not be empty';
                                }
                              }),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          child: DropdownButtonFormField<String>(
                            decoration: InputDecoration(
                              fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                              filled: true,
                              labelText: "Target Province",
                              prefixIcon: Icon(Icons.map_rounded),
                              contentPadding: EdgeInsets.all(3),
                              border: InputBorder.none,
                            ),
                            value: _targetProvinsi,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Target province should not be empty.';
                              }
                            },
                            isExpanded: true,
                            icon: const Icon(Icons.arrow_drop_down_rounded),
                            elevation: 16,
                            onChanged: (String? newValue) {
                              setState(() {
                                _targetProvinsi = newValue!;
                              });
                            },
                            items: _areas.keys
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(_areas[value]["area_name"]),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  AreaBansosHistory(_areas[_targetProvinsi]),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    width: 0.83 * displayWidth(context),
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 10.0,
                          offset: Offset(2.0, 2.0),
                        )
                      ],
                      color: Colors.white,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Bansos Type",
                            style: TextStyle(
                              fontWeight: FontWeight.w800,
                              fontSize: 23,
                              letterSpacing: -0.8,
                            )),
                        SizedBox(height: 5),
                        Row(
                          children: [
                            TextButton(
                              child: Text("Add Bansos Type"),
                              onPressed: () {
                                setState(() {
                                  numbOfBansos++;
                                  bansosControllers["type$numbOfBansos"] =
                                      TextEditingController();
                                  valueControllers["type$numbOfBansos"] =
                                      TextEditingController();
                                  unitControllers["type$numbOfBansos"] =
                                      TextEditingController();
                                });
                              },
                            ),
                            TextButton(
                              child: Text("Remove One Field"),
                              onPressed: () {
                                setState(() {
                                  if (numbOfBansos - 1 >= 0) {
                                    bansosControllers
                                        .remove("type$numbOfBansos");
                                    valueControllers
                                        .remove("type$numbOfBansos");
                                    unitControllers.remove("type$numbOfBansos");
                                    numbOfBansos--;
                                  }
                                });
                              },
                            ),
                          ],
                        ),
                        Scrollbar(
                            child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: DataTable(
                            columnSpacing: 15,
                            dataRowHeight: 60,
                            columns: [
                              DataColumn(label: Text("Assistance Type")),
                              DataColumn(label: Text("Valuer per Receiver")),
                              DataColumn(label: Text("Assistance Unit")),
                            ],
                            rows: [
                              for (int i = 1; i <= numbOfBansos; i++)
                                DataRow(cells: [
                                  DataCell(
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 5, horizontal: 0),
                                      child: TextFormField(
                                          controller:
                                              bansosControllers["type$i"],
                                          decoration: InputDecoration(
                                            fillColor: Color.fromRGBO(
                                                238, 238, 238, 0.93),
                                            filled: true,
                                            border: InputBorder.none,
                                          ),
                                          validator: (value) {
                                            if (value!.isEmpty) {
                                              return 'Type should not be empty';
                                            }
                                          }),
                                    ),
                                  ),
                                  DataCell(
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 5, horizontal: 0),
                                      child: TextFormField(
                                          controller:
                                              valueControllers["type$i"],
                                          decoration: InputDecoration(
                                            fillColor: Color.fromRGBO(
                                                238, 238, 238, 0.93),
                                            filled: true,
                                            border: InputBorder.none,
                                          ),
                                          validator: (value) {
                                            if (value!.isEmpty) {
                                              return 'Value should not be empty';
                                            } else if (double.tryParse(value) ==
                                                null) {
                                              return 'Value should only be of numeric type';
                                            }
                                          }),
                                    ),
                                  ),
                                  DataCell(
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 5, horizontal: 0),
                                      child: TextFormField(
                                          controller: unitControllers["type$i"],
                                          decoration: InputDecoration(
                                            fillColor: Color.fromRGBO(
                                                238, 238, 238, 0.93),
                                            filled: true,
                                            border: InputBorder.none,
                                          ),
                                          validator: (value) {
                                            if (value!.isEmpty) {
                                              return 'Unit should not be empty';
                                            }
                                          }),
                                    ),
                                  ),
                                ]),
                            ],
                          ),
                        )),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    width: 0.83 * displayWidth(context),
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 10.0,
                          offset: Offset(2.0, 2.0),
                        )
                      ],
                      color: Colors.white,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Admin Information",
                          style: TextStyle(
                            fontWeight: FontWeight.w800,
                            fontSize: 23,
                            letterSpacing: -0.8,
                          ),
                        ),
                        SizedBox(height: 10),
                        Text("Generating Admin",
                            style: TextStyle(
                              fontWeight: FontWeight.w800,
                              fontSize: 15,
                              letterSpacing: -0.8,
                            )),
                        SizedBox(height: 5),
                        Text(widget.user.telephone,
                            style: TextStyle(
                              fontWeight: FontWeight.w800,
                              fontSize: 18,
                              letterSpacing: -0.8,
                            )),
                        SizedBox(height: 10),
                        Text("Tracking Admin",
                            style: TextStyle(
                              fontWeight: FontWeight.w800,
                              fontSize: 15,
                              letterSpacing: -0.8,
                            )),
                        SizedBox(height: 5),
                        DropdownButtonFormField<String>(
                          decoration: InputDecoration(
                            fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                            filled: true,
                            prefixIcon: Icon(Icons.call),
                            contentPadding: EdgeInsets.all(12),
                            border: InputBorder.none,
                          ),
                          value: _trackingAdmin,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Tracking admin should not be empty";
                            }
                          },
                          isExpanded: true,
                          icon: const Icon(Icons.arrow_drop_down_rounded),
                          elevation: 16,
                          onChanged: (String? newValue) {
                            setState(() {
                              _trackingAdmin = newValue!;
                            });
                          },
                          items: _admins
                              .map<DropdownMenuItem<String>>((dynamic value) {
                            return DropdownMenuItem<String>(
                              value: value["telephone"],
                              child: Text(value["name"]),
                            );
                          }).toList(),
                        ),
                        SizedBox(
                          height: 7,
                        ),
                        Row(
                          children: [
                            if (showCircularProgress)
                              TextButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      Colors.green[400]),
                                  foregroundColor:
                                      MaterialStateProperty.all(Colors.white),
                                ),
                                child: Container(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                    strokeWidth: 3,
                                  ),
                                ),
                                onPressed: () {},
                              )
                            else
                              TextButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        Colors.green[800]),
                                    foregroundColor:
                                        MaterialStateProperty.all(Colors.white),
                                  ),
                                  child: Text("Generate"),
                                  onPressed: () async {
                                    if (_formKey.currentState!.validate()) {
                                      return showDialog<void>(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (BuildContext context) {
                                            return StatefulBuilder(
                                              builder: (context, innerSetState) {
                                                return AlertDialog(
                                                  title: Text("Confirmation"),
                                                  content: SingleChildScrollView(
                                                    child: Column(
                                                      children: [
                                                        Text(
                                                            "Do you wish to register a bansos to ${_targetUrbanVillage.text}, ${_targetSubDistrict.text}, ${_targetCity.text}, ${_areas[_targetProvinsi]["area_name"]}?"),
                                                      ],
                                                    ),
                                                  ),
                                                  actions: [
                                                    TextButton(
                                                      child: Text(
                                                        "Confirm",
                                                      ),
                                                      style: ButtonStyle(
                                                        backgroundColor: !showCircularProgress ? MaterialStateProperty.all(Theme.of(context).primaryColor) : MaterialStateProperty.all(Colors.grey),
                                                        foregroundColor: MaterialStateProperty.all(Colors.white),
                                                      ),
                                                      onPressed: () async {
                                                        
                                                        if(!showCircularProgress) {
                                                          
                                                          String? type;
                                                          String? value;
                                                          String? unit;

                                                          if (_formKey.currentState!
                                                              .validate()) {
                                                            for (int i = 1;
                                                                i <= numbOfBansos;
                                                                i++) {
                                                              type =
                                                                  bansosControllers[
                                                                          "type$i"]
                                                                      ?.text;
                                                              value =
                                                                  valueControllers[
                                                                          "type$i"]
                                                                      ?.text;
                                                              unit = unitControllers[
                                                                      "type$i"]
                                                                  ?.text;

                                                              bansosValue[type!] =
                                                                  value!;
                                                              bansosUnit[type] =
                                                                  unit!;
                                                            }

                                                            setState(() {
                                                              showCircularProgress =
                                                                  true;
                                                            });

                                                            innerSetState(() {
                                                              showCircularProgress =
                                                                  true;
                                                            });



                                                            // await Future.delayed(Duration(seconds: 10), () {});
                                                            
                                                            Map<String, dynamic>
                                                                response =
                                                                await sendBansos(
                                                                    _originUrbanVillage
                                                                        .text,
                                                                    _originSubDistrict
                                                                        .text,
                                                                    _originCity.text,
                                                                    "$_originProvinsi",
                                                                    _targetUrbanVillage
                                                                        .text,
                                                                    _targetSubDistrict
                                                                        .text,
                                                                    _targetCity.text,
                                                                    "$_targetProvinsi",
                                                                    bansosValue,
                                                                    bansosUnit,
                                                                    "$_trackingAdmin");

                                                            setState(() {
                                                              showCircularProgress =
                                                                  false;
                                                              if (response[
                                                                  "isSuccessful"]) {
                                                                bansosValue = {};
                                                                Navigator.pop(
                                                                    context);
                                                              } else {
                                                                final snackBar =
                                                                    SnackBar(
                                                                  content: Text(
                                                                      response[
                                                                          "error"]),
                                                                  behavior:
                                                                      SnackBarBehavior
                                                                          .floating,
                                                                  backgroundColor:
                                                                      Theme.of(context).primaryColor,
                                                                );
                                                                ScaffoldMessenger.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                        snackBar);
                                                              }
                                                            });
                                                            Navigator.pop(context,
                                                                'Bansos was registered successfully.');
                                                          }
                                                        } else {
                                                          
                                                        }
                                                      }
                                                        
                                                    ),
                                                    TextButton(
                                                        child: Text("Back",
                                                          style: TextStyle(
                                                            color: showCircularProgress ? Colors.grey : Theme.of(context).primaryColor,
                                                          ),
                                                        ),
                                                        onPressed: () {
                                                          if(!showCircularProgress) {
                                                            Navigator.pop(context);
                                                          } 
                                                          
                                                        }),
                                                  ],
                                                );
                                              }
                                            );
                                          });
                                    }
                                  }),
                            SizedBox(
                              width: 5,
                            ),
                            TextButton(
                              child: Text("Cancel"),
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Colors.amber[400]),
                                foregroundColor:
                                    MaterialStateProperty.all(Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              )
            ]),
          ),
        ));
  }
}

class AreaBansosHistory extends StatefulWidget {
  Map<String, dynamic>? area_info;

  AreaBansosHistory(this.area_info);

  @override
  _AreaBansosHistoryState createState() => _AreaBansosHistoryState();
}

class _AreaBansosHistoryState extends State<AreaBansosHistory> {
  bool showTable = false;

  @override
  Widget build(BuildContext context) {
    if (widget.area_info != null) {
      return Container(
        width: 0.83 * displayWidth(context),
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: Offset(2.0, 2.0),
            )
          ],
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Province Information",
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 23,
                letterSpacing: -0.8,
              ),
            ),
            Divider(),
            SizedBox(height: 20),
            Text("Area Code: ${widget.area_info!["area_code"]}"),
            SizedBox(height: 15),
            Text("Area Name: ${widget.area_info!["area_name"]}"),
            SizedBox(height: 15),
            Text(
                "Number of receiver: ${widget.area_info!["number_of_receiver"]}"),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  "Area Bansos History",
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 20,
                    letterSpacing: -0.8,
                  ),
                ),
                TextButton(
                  child: Text("Toggle View"),
                  onPressed: () {
                    setState(() {
                      showTable = !showTable;
                    });
                  },
                  style: ButtonStyle(
                      backgroundColor: !showTable
                          ? MaterialStateProperty.all(Theme.of(context).primaryColor)
                          : MaterialStateProperty.all(Colors.white),
                      foregroundColor: !showTable
                          ? MaterialStateProperty.all(Colors.white)
                          : MaterialStateProperty.all(Theme.of(context).primaryColor),
                      side: MaterialStateProperty.all(
                          BorderSide(color: Theme.of(context).primaryColor))),
                )
              ],
            ),
            if (showTable)
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: DataTable(
                  columns: [
                    DataColumn(
                        label: Text(
                      "Bansos ID",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    )),
                    DataColumn(
                        label: Text(
                      "Status",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    )),
                    DataColumn(
                        label: Text(
                      "Link",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    )),
                  ],
                  rows: [
                    for (int i = 0;
                        i < widget.area_info!["assigned_bansos"].length;
                        i++)
                      DataRow(cells: [
                        DataCell(
                            Text(widget.area_info!["assigned_bansos"][i][0])),
                        DataCell(
                            Text(widget.area_info!["assigned_bansos"][i][1])),
                        DataCell(
                          TextButton(
                            child: Text("More Info"),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BansosDetails(widget
                                        .area_info!["assigned_bansos"][i][0])),
                              );
                            },
                          ),
                        ),
                      ]),
                  ],
                ),
              )
          ],
        ),
      );
    } else {
      return Container(
        width: 0.83 * displayWidth(context),
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: Offset(2.0, 2.0),
            )
          ],
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Province Information",
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 23,
                letterSpacing: -0.8,
              ),
            ),
            Divider(),
            SizedBox(height: 20),
            Text("Area Code: "),
            SizedBox(height: 15),
            Text("Area Name: "),
            SizedBox(height: 15),
            Text("Number of receiver: "),
            SizedBox(height: 15),
            Text(
              "Area Bansos History",
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 20,
                letterSpacing: -0.8,
              ),
            ),
          ],
        ),
      );
    }
  }
}
