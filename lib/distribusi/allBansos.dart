// ignore_for_file: file_names

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/distribusi/registerArea.dart';
import 'package:bansospedulimobile/distribusi/adminBasedDrawer.dart';
import 'package:bansospedulimobile/distribusi/bansosDetail.dart';
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:bansospedulimobile/distribusi/generateBansos.dart';
import 'package:http/http.dart' as http;
import 'package:bansospedulimobile/distribusi/trackingAdminDashboard.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'dart:async';

Future<Map<String, dynamic>> fetchGroups(User user) async {
  const url =
      'https://bansos-peduli.herokuapp.com/distribusi/mob-get-bansos-groups/';

  try {
    Map<String, String> headers = {
      'Content-Type': 'application/json; charset=UTF-8',
    };
    Map<String, dynamic> body = {'sessionId': user.sessionId};

    final response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(body),
    );

    List<dynamic> extractedData = jsonDecode(response.body);

    // await Future.delayed(Duration(seconds: 10));
    if (response.statusCode == 200) {
      return {"isSuccessful": true, "data": extractedData, "error": null};
    } else {
      return {
        "isSuccessful": false,
        "data": extractedData,
        "error": "An error has occurred"
      };
    }
  } catch (error) {
    return {
      "isSuccessful": false,
      "data": [],
      "error": "Our web service is down."
    };
  }
}

class DistributionDashboard extends StatefulWidget {
  final User user;
  String? message;

  DistributionDashboard({required this.user, this.message});

  @override
  State<DistributionDashboard> createState() => _DistributionDashboardState();
}

class _DistributionDashboardState extends State<DistributionDashboard> {
  int _counter = 0;
  List<dynamic> _all_bansos = [];
  Map<String, dynamic> response = {};
  String? current_error;
  bool inRefresh = false;
  late Timer _timer;

  bool showQueued = true;
  bool showSent = true;
  bool showReceived = true;

  @override
  void initState() {
    super.initState();

    _timer = Timer.periodic(Duration(seconds: 8), (timer) async {
      await _intializeData();
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Future<void> _intializeData() async {
    response = await fetchGroups(widget.user);
    inRefresh = false;

    if (response["isSuccessful"]) {
      current_error = null;
      _all_bansos = response["data"];
    } else {
      current_error = response["error"];
    }
  }

  Future<void> _refresh() async {
    setState(() {
      inRefresh = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          elevation: 0,
          centerTitle: true,
          title: Text('Distribution Dashboard'),
          backgroundColor: Colors.white,
          foregroundColor: Theme.of(context).primaryColor,
        ),
        drawer: AdminBasedDrawer(widget.user),
        body: RefreshIndicator(
            onRefresh: _refresh,
            child: Column(
              children: [
                Container(
                  height: 60,
                  color: Colors.white,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                          child: Padding(
                            padding: EdgeInsets.all(5),
                            child: Text(
                              'Generate Bansos',
                            ),
                          ),
                          onPressed: () async {
                            final result = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        EntryBansos(widget.user)));

                            if (result != null) {
                              ScaffoldMessenger.of(context)
                                ..removeCurrentSnackBar()
                                ..showSnackBar(SnackBar(
                                  duration: Duration(seconds: 3),
                                  content: Text(result!),
                                  behavior: SnackBarBehavior.floating,
                                  backgroundColor: Theme.of(context).primaryColor,
                                ));

                              _refresh();
                            }
                          },
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            )),
                            backgroundColor:
                                MaterialStateProperty.all(Theme.of(context).primaryColor),
                            foregroundColor: MaterialStateProperty.all(Colors.white)
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        TextButton(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
                            child: Text('Register Area'),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      RegisterArea(widget.user)),
                            );
                          },
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25.0),
                              ),
                            ),
                            backgroundColor:
                                MaterialStateProperty.all(Theme.of(context).primaryColor),
                            foregroundColor: MaterialStateProperty.all(Colors.white),
                          ),
                        )
                      ]),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      child: Text("Queued"),
                      onPressed: () {
                        setState(() {
                          showQueued = !showQueued;
                        });
                      },
                      style: ButtonStyle(
                        foregroundColor: showQueued ? MaterialStateProperty.all(Colors.white) : MaterialStateProperty.all(Colors.red[900]),
                        backgroundColor: showQueued ? MaterialStateProperty.all(Colors.red[900]) : MaterialStateProperty.all(Colors.white),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            side: BorderSide(
                              color: Colors.red[900]!
                            ),
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(5), bottomLeft: Radius.circular(5))
                          )
                        )
                      ),
                    ),

                    TextButton(
                      child: Text("Sent"),
                      onPressed: () {
                        setState(() {
                          showSent = !showSent;
                        });
                      },

                      style: ButtonStyle(
                        foregroundColor: showSent ? MaterialStateProperty.all(Colors.white) : MaterialStateProperty.all(Colors.amber[800]),
                        backgroundColor: showSent ? MaterialStateProperty.all(Colors.amber[800]) : MaterialStateProperty.all(Colors.white),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            side: BorderSide(
                              color: Colors.amber[800]!,
                            )
                          )
                        )
                      ),
                    ),

                    TextButton(
                      child: Text("Received"),
                      onPressed: () {
                        setState(() {
                          showReceived = !showReceived;
                        });
                      },

                      style: ButtonStyle(
                        foregroundColor: showReceived ? MaterialStateProperty.all(Colors.white) : MaterialStateProperty.all(Colors.green[900]),
                        backgroundColor: showReceived ? MaterialStateProperty.all(Colors.green[900]) : MaterialStateProperty.all(Colors.white),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            side: BorderSide(
                              color: Colors.green[900]!
                            ),
                            borderRadius: BorderRadius.only(topRight: Radius.circular(5), bottomRight: Radius.circular(5))
                          )
                        )
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: ListView(
                    children: [
                      FutureBuilder(
                          future: _intializeData(),
                          builder: (context, snapshot) {
                            if (_all_bansos.isNotEmpty ||
                                snapshot.connectionState ==
                                    ConnectionState.done) {
                              if (current_error != null) {
                                return Container(
                                    width: 0.75 * displayWidth(context),
                                    child: Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        margin: EdgeInsets.only(
                                            top: displayHeight(context) * 0.1),
                                        elevation: 0,
                                        child: Padding(
                                          padding: EdgeInsets.all(20),
                                          child: Column(children: [
                                            Icon(
                                              Icons
                                                  .signal_wifi_statusbar_connected_no_internet_4_rounded,
                                              size: 200,
                                              color: Theme.of(context).primaryColor,
                                            ),
                                            Text(
                                              "Sorry :(",
                                              style: TextStyle(
                                                color: Theme.of(context).primaryColor,
                                                fontSize: 30,
                                                fontWeight: FontWeight.bold,
                                                letterSpacing: -0.8,
                                              ),
                                            ),
                                            SizedBox(height: 10),
                                            Text(
                                              current_error!,
                                              style: TextStyle(
                                                color: Colors.grey.shade700,
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                letterSpacing: -0.8,
                                              ),
                                            ),
                                            !inRefresh
                                                ? TextButton(
                                                    child: Text("Try Again."),
                                                    onPressed: () {
                                                      setState(() {});
                                                    },
                                                    style: ButtonStyle(
                                                      minimumSize:
                                                          MaterialStateProperty
                                                              .all(Size(
                                                                  double
                                                                      .infinity,
                                                                  30)),
                                                      backgroundColor:
                                                          MaterialStateProperty
                                                              .all(Theme.of(context).primaryColor),
                                                      foregroundColor:
                                                          MaterialStateProperty
                                                              .all(
                                                                  Colors.white),
                                                    ),
                                                  )
                                                : TextButton(
                                                    child: Container(
                                                      height: 20,
                                                      width: 20,
                                                      child:
                                                          CircularProgressIndicator(
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                    onPressed: () {},
                                                    style: ButtonStyle(
                                                      minimumSize:
                                                          MaterialStateProperty
                                                              .all(Size(
                                                                  double
                                                                      .infinity,
                                                                  30)),
                                                      backgroundColor:
                                                          MaterialStateProperty
                                                              .all(Theme.of(context).primaryColor),
                                                      foregroundColor:
                                                          MaterialStateProperty
                                                              .all(
                                                                  Colors.white),
                                                    ),
                                                  ),
                                          ]),
                                        )));
                              }

                              if (_all_bansos.length > 0) {
                                return Container(
                                  margin: EdgeInsets.fromLTRB(
                                      0.0 * displayWidth(context), 0, 0.065, 0),
                                  child: Column(
                                    children: [
                                      if (inRefresh) LinearProgressIndicator(),

                                      if(showQueued)
                                      for (int i = 0;
                                          i < _all_bansos.length;
                                          i++)
                                        if (_all_bansos[i]["fields"]
                                                ["status"] ==
                                            'queued')
                                          BansosCards(
                                              _all_bansos[i]["fields"]
                                                  ["status"],
                                              _all_bansos[i]["pk"],
                                              _all_bansos[i]["fields"]
                                                  ["destination_kelurahan"],
                                              _all_bansos[i]["fields"]
                                                  ["destination_kecamatan"],
                                              _all_bansos[i]["fields"][
                                                  "destination_kabupaten_kota"],
                                              _all_bansos[i]["fields"]
                                                  ["receiving_provinsi"],
                                              _all_bansos[i]["fields"]
                                                  ["total_receiver"]),

                                      if(showSent)
                                      for (int i = 0;
                                          i < _all_bansos.length;
                                          i++)
                                        if (_all_bansos[i]["fields"]
                                                ["status"] ==
                                            'sent')
                                          BansosCards(
                                              _all_bansos[i]["fields"]
                                                  ["status"],
                                              _all_bansos[i]["pk"],
                                              _all_bansos[i]["fields"]
                                                  ["destination_kelurahan"],
                                              _all_bansos[i]["fields"]
                                                  ["destination_kecamatan"],
                                              _all_bansos[i]["fields"][
                                                  "destination_kabupaten_kota"],
                                              _all_bansos[i]["fields"]
                                                  ["receiving_provinsi"],
                                              _all_bansos[i]["fields"]
                                                  ["total_receiver"]),

                                      if(showReceived)
                                      for (int i = 0;
                                          i < _all_bansos.length;
                                          i++)
                                        if (_all_bansos[i]["fields"]
                                                ["status"] ==
                                            'received')
                                          BansosCards(
                                              _all_bansos[i]["fields"]
                                                  ["status"],
                                              _all_bansos[i]["pk"],
                                              _all_bansos[i]["fields"]
                                                  ["destination_kelurahan"],
                                              _all_bansos[i]["fields"]
                                                  ["destination_kecamatan"],
                                              _all_bansos[i]["fields"][
                                                  "destination_kabupaten_kota"],
                                              _all_bansos[i]["fields"]
                                                  ["receiving_provinsi"],
                                              _all_bansos[i]["fields"]
                                                  ["total_receiver"]),
                                    ],
                                  ),
                                );
                              } else {
                                return Container(
                                  margin: EdgeInsets.only(
                                      top: 0.1 * displayHeight(context)),
                                  child: Column(
                                    children: [
                                      Icon(
                                        Icons.warning_rounded,
                                        size: 100,
                                      ),
                                      Text(
                                          "It appears that no bansos has been registered."),
                                    ],
                                  ),
                                );
                              }
                            } else if (snapshot.hasError) {
                              return Center(
                                child: Column(children: [
                                  Icon(Icons.warning),
                                  Text("We are sorry. An error has occurred."),
                                ]),
                              );
                            } else {
                              return LinearProgressIndicator();
                            }
                          }),
                    ],
                  ),
                ),
              ],
            ))
        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}

class BansosCards extends StatelessWidget {
  Color? finalStateColor;
  String? uuid;
  String? destinationKelurahan;
  String? destinationKecamatan;
  String? destinationKabKota;
  String? destinationProvinsi;
  int? total_receiver;

  BansosCards(
      String status,
      this.uuid,
      this.destinationKelurahan,
      this.destinationKecamatan,
      this.destinationKabKota,
      this.destinationProvinsi,
      this.total_receiver) {
    if (status == 'received') {
      finalStateColor = Colors.green[900];
    } else if (status == 'sent') {
      finalStateColor = Colors.amber[800];
    } else {
      finalStateColor = Colors.red[900];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 20),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 15.0,
              offset: Offset(2.0, 4.0),
            )
          ],
        ),
        child: Column(
          children: [
            Container(
              width: 0.83 * displayWidth(context),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
                color: finalStateColor,
              ),
              child: Text(
                'Bansos to $destinationProvinsi',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
              padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Text(uuid!),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              padding: EdgeInsets.fromLTRB(10, 0, 0, 10),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Text(
                  "Destination of this bansos is $total_receiver citizens in $destinationProvinsi."),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Text(
                  "This bansos will be sent to $destinationKelurahan, $destinationKecamatan, $destinationKabKota, $destinationProvinsi."),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
                color: Colors.white,
              ),
              child: FlatButton(
                  color: Colors.blueAccent[700],
                  child: Text(
                    "Bansos Details",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => BansosDetails("$uuid")),
                    );
                  }),
            ),
          ],
        ));
  }
}
