import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:bansospedulimobile/distribusi/generateBansos.dart';
import 'package:bansospedulimobile/distribusi/bansosDetail.dart'
    as bansosDetail;
import 'package:http/http.dart' as http;

Future<Map<String, dynamic>> sendNewLocation(
    String bansos_id,
    String new_kelurahan,
    String new_kecamatan,
    String new_city,
    String new_province,
    String status) async {
  final url =
      'https://bansos-peduli.herokuapp.com/distribusi/mob-add-prev-location/';

  try {
    final response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'group-id': bansos_id,
        'kelurahan': new_kelurahan,
        'kecamatan': new_kecamatan,
        'kabkota': new_city,
        'provinsi': new_province,
        'status': status,
      }),
    );

    Map<String, dynamic> result = jsonDecode(response.body);

    if (response.statusCode == 200) {
      return result;
    } else {
      return <String, dynamic>{'error': 'Web service is offline'};
    }
  } catch (error) {
    return {'isSuccessful': false, 'error': 'Web service is offline'};
  }
}

class UpdateShipping extends StatefulWidget {
  String uuid;

  UpdateShipping(this.uuid);

  @override
  _UpdateShippingState createState() => _UpdateShippingState();
}

class _UpdateShippingState extends State<UpdateShipping> {
  Map<String, dynamic> _areas = {};
  Map<String, dynamic> _bansos_data = {};

  Future<void> fillAreas() async {
    _areas = await fetchArea();
    _bansos_data = await bansosDetail.fetchBansos(widget.uuid);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Column(
          children: [
            Row(
              children: [
                Icon(
                  Icons.pin_drop,
                ),
                Text('BansosPeduli'),
              ],
            ),
          ],
        ),
        elevation: 0,
      ),
      body: Column(
        children: [
          Container(
            color: Theme.of(context).primaryColor,
            width: double.infinity,
            child: Text(
              "Update Shipping Information",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                letterSpacing: -0.8,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            color: Theme.of(context).primaryColor,
            width: double.infinity,
            padding: EdgeInsets.all(8.0),
            child: Text(
              "ID Group Bansos",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 23,
                letterSpacing: -0.8,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            color: Theme.of(context).primaryColor,
            width: double.infinity,
            padding: EdgeInsets.all(12.0),
            child: Text(
              "#" + widget.uuid,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18,
                letterSpacing: -0.8,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
          Expanded(
              child: ListView(
            children: [
              Column(
                children: [
                  FutureBuilder(
                    future: fillAreas(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        // Empty bansos data implies a problem in fetching the corresponding details
                        if (_bansos_data.isEmpty) {
                          return Container(
                            margin: EdgeInsets.only(
                                top: 0.1 * displayWidth(context)),
                            child: Column(
                              children: [
                                Icon(
                                  Icons.warning_sharp,
                                  size: 50.0,
                                ),
                                Text(
                                  "Something went wrong.",
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                ),
                                TextButton(
                                    child: Text("Try again"),
                                    onPressed: () {
                                      setState(() {});
                                    }),
                              ],
                            ),
                          );
                        }
                        return Column(
                          children: [
                            SizedBox(height: 20),
                            ShippingForm(_areas, widget.uuid),
                            SizedBox(height: 20),
                            bansosDetail.AreaInfo(
                              _bansos_data["area"]["area_code"],
                              _bansos_data["area"]["des_urban_village"],
                              _bansos_data["area"]["des_subdistrict"],
                              _bansos_data["area"]["des_city"],
                              _bansos_data["area"]["des_provinsi"],
                              _bansos_data["previous_locations"],
                            ),
                          ],
                        );
                      } else {
                        return LinearProgressIndicator(color: Theme.of(context).primaryColor);
                      }
                    },
                  ),
                ],
              ),
            ],
          ))
        ],
      ),
    );
  }
}

class ShippingForm extends StatefulWidget {
  Map<String, dynamic> _areas;
  String uuid;
  ShippingForm(this._areas, this.uuid);

  @override
  _ShippingFormState createState() => _ShippingFormState();
}

class _ShippingFormState extends State<ShippingForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController new_kelurahan = TextEditingController();
  TextEditingController new_kecamatan = TextEditingController();
  TextEditingController new_city = TextEditingController();

  Map<String, String> statusMap = {
    "queued": "QUEUED",
    "sent": "SENT",
    "received": "RECEIVED",
  };

  Map<String, dynamic>? fetchedResult;
  String? _currentProvinsi;
  String? _status;
  bool? isSuccessful;
  bool showCircularProgress = false;
  List<dynamic> errors = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      width: 0.85 * displayWidth(context),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 9.0,
            offset: Offset(4.0, 4.0),
          )
        ],
      ),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // if(errors.isNotEmpty)
            // Container(
            //   color: Colors.red[100],
            //   padding: EdgeInsets.all(5),
            //   child: Column(
            //     children: [
            //       for(int i=0; i<errors.length; i++)
            //         Text(
            //           errors[i],
            //           textAlign: TextAlign.center,
            //         )
            //     ],
            //   ),
            // ),
            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
              child: Text(
                "Urban Village",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  letterSpacing: -0.8,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
              child: TextFormField(
                controller: new_kelurahan,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blueAccent,
                    ),
                  ),
                  prefixIcon: Padding(
                    padding: const EdgeInsetsDirectional.only(start: 3.0),
                    child: Icon(Icons
                        .location_on_outlined), // myIcon is a 48px-wide widget.
                  ),
                  contentPadding: EdgeInsets.all(3),
                  hintText: "Enter Kelurahan",
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Urban Village should not be empty.";
                  } else {
                    return null;
                  }
                },
              ),
            ),

            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
              child: Text(
                "Sub-District",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  letterSpacing: -0.8,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
              child: TextFormField(
                controller: new_kecamatan,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blueAccent,
                    ),
                  ),
                  prefixIcon: Padding(
                    padding: const EdgeInsetsDirectional.only(start: 3.0),
                    child: Icon(Icons
                        .other_houses_sharp), // myIcon is a 48px-wide widget.
                  ),
                  contentPadding: EdgeInsets.all(3),
                  hintText: "Enter Kecamatan",
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Sub-District should not be empty.";
                  } else {
                    return null;
                  }
                },
              ),
            ),

            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
              child: Text(
                "District/City",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  letterSpacing: -0.8,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
              child: TextFormField(
                controller: new_city,
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blueAccent,
                    ),
                  ),
                  prefixIcon: Padding(
                    padding: const EdgeInsetsDirectional.only(start: 3.0),
                    child: Icon(Icons
                        .location_city_sharp), // myIcon is a 48px-wide widget.
                  ),
                  contentPadding: EdgeInsets.all(3),
                  hintText: "Enter Kabupaten Kota",
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black26,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "District/City should not be empty.";
                  } else {
                    return null;
                  }
                },
              ),
            ),

            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
              child: Text(
                "Current Province",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  letterSpacing: -0.8,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
              child: DropdownButtonFormField<String>(
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  prefixIcon: Icon(Icons.map_rounded),
                  contentPadding: EdgeInsets.all(3),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black12,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Province should not be empty";
                  } else {
                    return null;
                  }
                },
                value: _currentProvinsi,
                isExpanded: true,
                icon: const Icon(Icons.arrow_drop_down_rounded),
                elevation: 16,
                onChanged: (String? newValue) {
                  setState(() {
                    _currentProvinsi = newValue!;
                  });
                },
                items: widget._areas.keys
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(widget._areas[value]["area_name"]),
                  );
                }).toList(),
              ),
            ),

            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
              child: Text(
                "Status",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  letterSpacing: -0.8,
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
              child: DropdownButtonFormField<String>(
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  prefixIcon: Icon(Icons.arrow_forward_rounded),
                  contentPadding: EdgeInsets.all(3),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black12,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Status should not be empty";
                  } else {
                    return null;
                  }
                },
                value: _status,
                isExpanded: true,
                icon: const Icon(Icons.arrow_drop_down_rounded),
                elevation: 16,
                onChanged: (String? newValue) {
                  setState(() {
                    _status = newValue!;
                  });
                },
                items: statusMap.keys
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(statusMap[value]! + ""),
                  );
                }).toList(),
              ),
            ),

            Padding(
              padding: EdgeInsets.all(10.0),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (showCircularProgress)
                    TextButton(
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularProgressIndicator(
                            color: Colors.white,
                            strokeWidth: 3,
                          ),
                        ),
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.blueAccent[200]),
                        ),
                        onPressed: () {})
                  else
                    TextButton(
                      child: Text(
                        "Save",
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blueAccent[700]),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          setState(() {
                            showCircularProgress = true;
                          });

                          fetchedResult = await sendNewLocation(
                              widget.uuid,
                              new_kelurahan.text,
                              new_kecamatan.text,
                              new_city.text,
                              "$_currentProvinsi",
                              "$_status");

                          isSuccessful = await fetchedResult!["isSuccessful"];

                          if (isSuccessful!) {
                            Navigator.pop(context, 'Bansos Group was Edited.');
                          } else {
                            setState(() {
                              showCircularProgress = false;
                              final snackBar = SnackBar(
                                content: Text(fetchedResult!["error"]),
                                behavior: SnackBarBehavior.floating,
                                backgroundColor: Theme.of(context).primaryColor,
                              );

                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                            });
                          }
                        } else {}
                      },
                    ),
                  SizedBox(width: 5),
                  TextButton(
                    child: Text(
                      "Cancel",
                      style: TextStyle(color: Colors.white),
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          showCircularProgress ? MaterialStateProperty.all(Colors.grey) :
                          MaterialStateProperty.all(Colors.red[700]),
                    ),
                    onPressed: () {
                      showCircularProgress ? null : Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
