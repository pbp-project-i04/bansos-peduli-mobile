import 'dart:convert';
import 'dart:async';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:http/http.dart' as http;
import 'package:bansospedulimobile/distribusi/shippingHistory.dart';
import 'package:bansospedulimobile/distribusi/updateShipping.dart';
import 'package:bansospedulimobile/distribusi/adminBasedDrawer.dart';

Future<Map<String, dynamic>> get_corresponding_bansos(User user) async {
  String url =
      'https://bansos-peduli.herokuapp.com/distribusi/mob-get-trackings/';

  try {
    final response = await http.post(Uri.parse(url),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          'session_id': user.sessionId,
        }));
    // await Future.delayed((Duration(seconds: 10)));

    List<dynamic> extractedData = jsonDecode(response.body);

    if (response.statusCode == 200) {
      return {"isSuccessful": true, "data": extractedData, "error": ""};
    } else {
      return {
        "isSuccessful": false,
        "data": [],
        "error": "We are sorry. An error has occurred."
      };
    }
  } catch (error) {
    return {
      "isSuccessful": false,
      "data": [],
      "error": "Our web service is down."
    };
  }
}

class TrackingAdminDashboard extends StatefulWidget {
  User user;

  TrackingAdminDashboard(this.user);

  @override
  _TrackingAdminDashboardState createState() => _TrackingAdminDashboardState();
}

class _TrackingAdminDashboardState extends State<TrackingAdminDashboard> {
  List<dynamic> _bansos_groups = [];
  int _current_bansos_length = 0;
  String? current_error;
  bool hasNewBansos = false;
  bool firstCall = true;
  bool inRefresh = false;
  late Timer timer;

  void showUpdate(BuildContext context) {
    final snackBar = SnackBar(
      content:
          Text("A bansos was just assigned to you. Refresh the page to view."),
      behavior: SnackBarBehavior.floating,
      backgroundColor: Theme.of(context).primaryColor,
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Future<void> initialCall(BuildContext context) async {
    firstCall = false;
    Map<String, dynamic> result = await get_corresponding_bansos(widget.user);
    _bansos_groups = result["data"];
    _current_bansos_length = _bansos_groups.length;

    timer = Timer.periodic(Duration(seconds: 8), (timer) async {
      Map<String, dynamic> result = await get_corresponding_bansos(widget.user);

      if (result["isSuccessful"] &&
          result["data"].length > _current_bansos_length) {
        _bansos_groups = result["data"];
        _current_bansos_length = _bansos_groups.length;
        if (mounted) {
          showUpdate(context);
        }
      }
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  Future<void> fillGroups() async {
    setState(() {
      inRefresh = true;
    });

    Map<String, dynamic> result = await get_corresponding_bansos(widget.user);
    inRefresh = false;

    if (result["isSuccessful"]) {
      _bansos_groups = result["data"];
    } else {
      current_error = result["error"];
    }
  }

  Future<void> passableSetState() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Column(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.pin_drop,
                  ),
                  Text('BansosPeduli'),
                ],
              ),
            ],
          ),
          elevation: 0,
        ),
        drawer: AdminBasedDrawer(widget.user),
        body: RefreshIndicator(
          onRefresh: passableSetState,
          child: Column(
            children: [
              Container(
                width: double.infinity,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    "Tracking Admin Dashboard",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                color: Theme.of(context).primaryColor,
              ),
              Expanded(
                child: ListView(
                  children: [
                    FutureBuilder(
                        future: firstCall ? initialCall(context) : fillGroups(),
                        builder: (context, snapshot) {
                          if (_bansos_groups.isNotEmpty ||
                              snapshot.connectionState == ConnectionState.done) {
                            if (current_error != null) {
                              _bansos_groups = [];
                              return Container(
                                width: 0.75 * displayWidth(context),
                                padding: EdgeInsets.all(20),
                                margin: EdgeInsets.only(
                                    top: displayHeight(context) * 0.1),
                                child: Column(children: [
                                  Icon(
                                    Icons
                                        .signal_wifi_statusbar_connected_no_internet_4_rounded,
                                    size: 200,
                                    color: Theme.of(context).colorScheme.primary,
                                  ),
                                  Text(
                                    "Sorry :(",
                                    style: TextStyle(
                                      color: Theme.of(context).colorScheme.primary,
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: -0.8,
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    current_error!,
                                    style: TextStyle(
                                      color: Colors.grey.shade700,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: -0.8,
                                    ),
                                  ),
                                  TextButton(
                                    child: Text("Try Again."),
                                    onPressed: () {
                                      // fillGroups();

                                      setState(() {
                                        current_error = null;
                                      });
                                    },
                                    style: ButtonStyle(
                                      minimumSize: MaterialStateProperty.all(
                                          Size(double.infinity, 30)),
                                      backgroundColor:
                                          MaterialStateProperty.all(Theme.of(context).colorScheme.primary),
                                      foregroundColor:
                                          MaterialStateProperty.all(Colors.white),
                                    ),
                                  ),
                                ]),
                              );
                            } else if (_bansos_groups.isNotEmpty) {
                              return Column(
                                children: [
                                  if (inRefresh) LinearProgressIndicator(color: Theme.of(context).primaryColor),
                                  for (int i = 0; i < _bansos_groups.length; i++)
                                    if (_bansos_groups[i]["fields"]["status"] !=
                                        'received')
                                      TrackingCards(
                                          _bansos_groups[i]["fields"]["status"],
                                          _bansos_groups[i]["pk"],
                                          _bansos_groups[i]["fields"]
                                              ["destination_kelurahan"],
                                          _bansos_groups[i]["fields"]
                                              ["destination_kecamatan"],
                                          _bansos_groups[i]["fields"]
                                              ["destination_kabupaten_kota"],
                                          _bansos_groups[i]["fields"]
                                              ["receiving_provinsi"],
                                          _bansos_groups[i]["fields"]
                                              ["total_receiver"],
                                          _bansos_groups[i]["fields"]
                                              ["time_stamp"],
                                          passableSetState),
                                  for (int i = 0; i < _bansos_groups.length; i++)
                                    if (_bansos_groups[i]["fields"]["status"] ==
                                        'received')
                                      TrackingCards(
                                          _bansos_groups[i]["fields"]["status"],
                                          _bansos_groups[i]["pk"],
                                          _bansos_groups[i]["fields"]
                                              ["destination_kelurahan"],
                                          _bansos_groups[i]["fields"]
                                              ["destination_kecamatan"],
                                          _bansos_groups[i]["fields"]
                                              ["destination_kabupaten_kota"],
                                          _bansos_groups[i]["fields"]
                                              ["receiving_provinsi"],
                                          _bansos_groups[i]["fields"]
                                              ["total_receiver"],
                                          _bansos_groups[i]["fields"]
                                              ["time_stamp"],
                                          passableSetState),
                                ],
                              );
                            } else {
                              return Container(
                                margin: EdgeInsets.only(
                                    top: 0.1 * displayHeight(context)),
                                child: Column(
                                  children: [
                                    Icon(
                                      Icons.warning_rounded,
                                      size: 100,
                                    ),
                                    Text(
                                        "It appears that no bansos has been assigned to you."),
                                  ],
                                ),
                              );
                            }
                          } else {
                            return LinearProgressIndicator();
                          }
                        }),
                  ],
                ),
              )
            ],
          ),
        ),
    );
  }
}

class TrackingCards extends StatefulWidget {
  String? uuid;
  String? time_stamp;
  String? destinationKelurahan;
  String? destinationKecamatan;
  String? destinationKabKota;
  String? destinationProvinsi;
  int? total_receiver;
  String? status;
  Function parent_refresh;

  TrackingCards(
      this.status,
      this.uuid,
      this.destinationKelurahan,
      this.destinationKecamatan,
      this.destinationKabKota,
      this.destinationProvinsi,
      this.total_receiver,
      this.time_stamp,
      this.parent_refresh);

  @override
  _TrackingCardsState createState() => _TrackingCardsState();
}

class _TrackingCardsState extends State<TrackingCards> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 20),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 15.0,
              offset: Offset(2.0, 4.0),
            )
          ],
        ),
        child: Column(
          children: [
            Container(
              width: 0.83 * displayWidth(context),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
                color: Colors.blue[900],
              ),
              child: Text(
                '${widget.status}',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
              padding: EdgeInsets.fromLTRB(10, 15, 10, 10),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Text(
                "${widget.destinationProvinsi} - ${widget.time_stamp}",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  letterSpacing: -0.8,
                ),
              ),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              padding: EdgeInsets.fromLTRB(10, 0, 0, 10),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Text(
                  "Destination of this bansos is ${widget.total_receiver} citizens in ${widget.destinationProvinsi}."),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Text(
                  "This bansos will be sent to ${widget.destinationKelurahan}, ${widget.destinationKecamatan}, ${widget.destinationKabKota}, ${widget.destinationProvinsi}."),
            ),
            if (widget.status == 'queued' || widget.status == 'sent')
              Container(
                width: 0.83 * displayWidth(context),
                padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                  color: Colors.white,
                ),
                child: FlatButton(
                  color: Colors.blueAccent[700],
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Update",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Icon(
                            Icons.history,
                            color: Colors.white,
                          ),
                        ]),
                  ),
                  onPressed: () async {
                    var result = await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                UpdateShipping("${widget.uuid}")));

                    if (result != null) {
                      ScaffoldMessenger.of(context)
                        ..removeCurrentSnackBar()
                        ..showSnackBar(SnackBar(
                          content: Text(result!),
                          behavior: SnackBarBehavior.floating,
                          backgroundColor: Theme.of(context).primaryColor,
                        ));

                      widget.parent_refresh();
                    }
                  },
                ),
              ),
            if (widget.status == 'received')
              Container(
                width: 0.83 * displayWidth(context),
                padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                  color: Colors.white,
                ),
                child: FlatButton(
                  color: Colors.red[900],
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "History",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Icon(
                            Icons.history,
                            color: Colors.white,
                          ),
                        ]),
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ShippingHistory("${widget.uuid}")));
                  },
                ),
              ),
          ],
        ));
  }
}
