import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:bansospedulimobile/distribusi/generateBansos.dart';
import 'package:bansospedulimobile/distribusi/bansosDetail.dart' as bansosDetail;
import 'package:http/http.dart' as http;

class ShippingHistory extends StatefulWidget {
  String uuid;

  ShippingHistory(this.uuid);

  @override
  _ShippingHistoryState createState() => _ShippingHistoryState();
}

class _ShippingHistoryState extends State<ShippingHistory> {
  Map<String, dynamic> _areas = {};
  Map<String, dynamic> _bansos_data = {};

  Future<void> fillAreas() async {
    _bansos_data = await bansosDetail.fetchBansos(widget.uuid);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Column(
          children: [
            Row(
              children: [
                Icon(
                  Icons.pin_drop,
                ),
                Text('BansosPeduli'),
              ],
            ),
          ],
          
        ),
        elevation: 0,
      ),

      body: Column(
        children: [
          Container(
                  color: Theme.of(context).primaryColor,
                  width: double.infinity,
                  child: Text(
                    "History Shipping Information",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      letterSpacing: -0.8,
                      color: Colors.white,
                    ),
                  ),
                ),

                Container(
                  color: Theme.of(context).primaryColor,
                  width: double.infinity,
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "ID Group Bansos",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 23,
                      letterSpacing: -0.8,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),

                Container(
                  color: Theme.of(context).primaryColor,
                  width: double.infinity,
                  padding: EdgeInsets.all(12.0),
                  child: Text(
                    "#" + widget.uuid,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18,
                      letterSpacing: -0.8,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),

          Expanded(
            child: ListView(
            children: [
              FutureBuilder(
                  future: fillAreas(),
                  builder: (context, snapshot) {
                    if(snapshot.connectionState == ConnectionState.done) {
                      if(_bansos_data.isEmpty) {
                        return Container(
                          margin: EdgeInsets.only(top: 0.1*displayWidth(context)),
                          child: Column(
                            children: [
                              Icon(
                                Icons.warning_sharp,
                                size: 50.0,
                              ),
                              Text(
                                "Something went wrong.",
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                              TextButton(
                                child: Text("Try again"),
                                onPressed: () {
                                  setState(() {});
                                }
                              ),
                            ],
                          ),
                        );
                      }
                      return Column(
                        children: [
                          SizedBox(height: 20),
                          bansosDetail.AreaInfo(
                            _bansos_data["area"]["area_code"],
                            _bansos_data["area"]["des_urban_village"],
                            _bansos_data["area"]["des_subdistrict"],
                            _bansos_data["area"]["des_city"],
                            _bansos_data["area"]["des_provinsi"],
                            _bansos_data["previous_locations"],
                          ),
                        ],
                      );
                    } else {
                      return LinearProgressIndicator();
                    }
                  },
                ),
            ],
          ),
          )
        ],
      ),
    );
  }
}


