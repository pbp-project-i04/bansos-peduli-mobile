// ignore_for_file: file_names

import 'dart:convert';
import 'dart:ui';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:http/http.dart' as http;

Future<Map<String, dynamic>> fetchBansos(String bansosid) async {
  String url =
      'https://bansos-peduli.herokuapp.com/distribusi/mob-bansos-from-id/?bansos-id=$bansosid';

  try {
    final response = await http.get(Uri.parse(url));
    Map<String, dynamic> extractedData = jsonDecode(response.body);

    // await Future.delayed(Duration(seconds: 5));
    return extractedData;
  } catch (error) {

    return {};
  }
}

class BansosDetails extends StatefulWidget {
  String bansos_code;
  BansosDetails(this.bansos_code);

  @override
  _BansosDetailsState createState() => _BansosDetailsState();
}

class _BansosDetailsState extends State<BansosDetails> {
  Map<String, dynamic> _bansos_data = {};
  late Timer timer;

  @override
  void initState() {
    super.initState();

    timer = Timer.periodic(Duration(seconds: 8), (timer) async {
      await fillBansos();
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  Future<void> fillBansos() async {
    Map<String, dynamic> result = await fetchBansos(widget.bansos_code);
    _bansos_data = result;
  }

  Future<void> _refresh() async {
    setState(() {});
  }

  // This widget is the root of the application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        title: Row(
          children: [
            Icon(
              Icons.pin_drop,
            ),
            Text('BansosPeduli'),
          ],
        ),
      ),
      // drawer: AdminBasedDrawer(global_admin_type),

      body: RefreshIndicator(
        onRefresh: _refresh,
        child: Column(
          children: [
            Container(
              height: 40,
              width: displayWidth(context),
              color: Theme.of(context).primaryColor,
              child: Text(
                "Detail Bansos",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              height: 40,
              width: displayWidth(context),
              color: Theme.of(context).primaryColor,
              child: Text(
                widget.bansos_code,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Expanded(
              child: ListView(
                children: [
                  FutureBuilder(
                    future: fillBansos(),
                    builder: (context, snapshot) {
                      if (_bansos_data.isNotEmpty ||
                          snapshot.connectionState == ConnectionState.done) {
                        if (_bansos_data.isEmpty) {
                          return Container(
                            margin: EdgeInsets.symmetric(
                                vertical: 0.2 * displayHeight(context)),
                            child: Column(
                              children: [
                                Icon(
                                  Icons.warning_sharp,
                                  size: 50.0,
                                ),
                                Text(
                                  "Something went wrong.",
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                ),
                                TextButton(
                                    child: Text("Try again"),
                                    onPressed: () {
                                      setState(() {});
                                    }),
                              ],
                            ),
                          );
                        }
                        return Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            TrackingAdminInfo(_bansos_data["admin"]["name"],
                                _bansos_data["admin"]["telephone"]),
                            SizedBox(
                              height: 20,
                            ),
                            AreaInfo(
                              _bansos_data["area"]["area_code"],
                              _bansos_data["area"]["des_urban_village"],
                              _bansos_data["area"]["des_subdistrict"],
                              _bansos_data["area"]["des_city"],
                              _bansos_data["area"]["des_provinsi"],
                              _bansos_data["previous_locations"],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            PackageInfo(
                                _bansos_data["bentuk_bantuan"],
                                _bansos_data["satuan_bantuan"],
                                _bansos_data["total_receiver"]),
                            SizedBox(
                              height: 5,
                            ),
                            ElevatedButton(
                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Theme.of(context).primaryColor)),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text("Back"),
                            )
                          ],
                        );
                      } else {
                        return Column(
                          children: [
                            Container(
                              width: 0.5 * displayWidth(context),
                              margin: EdgeInsets.symmetric(
                                horizontal: 0.25 * displayWidth(context),
                                vertical: 0.75 * displayWidth(context),
                              ),
                              child: LinearProgressIndicator(color: Theme.of(context).primaryColor),
                            ),
                          ],
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TrackingAdminInfo extends StatelessWidget {
  String tracking_name;
  String tracking_telephone;

  TrackingAdminInfo(this.tracking_name, this.tracking_telephone);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 0.83 * displayWidth(context),
      height: 100,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: Offset(2.0, 2.0),
          )
        ],
        color: Colors.white,
      ),
      child: Padding(
        padding: EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Tracking Admin",
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 20,
                letterSpacing: -0.8,
              ),
            ),
            SizedBox(height: 5),
            Text(
              "Name: $tracking_name",
              style: TextStyle(
                fontSize: 15,
              ),
            ),
            SizedBox(height: 5),
            Text(
              "Telephone: $tracking_telephone",
              style: TextStyle(
                fontSize: 15,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AreaInfo extends StatelessWidget {
  String area_code;
  String des_urban_village;
  String des_subdistrict;
  String des_city;
  String des_province;
  List<dynamic> previous_locations;

  AreaInfo(
    this.area_code,
    this.des_urban_village,
    this.des_subdistrict,
    this.des_city,
    this.des_province,
    this.previous_locations,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 0.83 * displayWidth(context),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 12.0,
                offset: Offset(2.0, 0.0),
              ),
            ]),
        child: Column(
          children: [
            Container(
              width: 0.83 * displayWidth(context),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  topRight: Radius.circular(15.0),
                ),
                color: Colors.lightBlue[100],
              ),
              padding: EdgeInsets.all(15),
              child: Text(
                "Area Information",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  letterSpacing: -0.8,
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Destination Area",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        letterSpacing: -0.8,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Area-Code: $area_code",
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Urban Village Destination: $des_urban_village",
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Sub-District Destination: $des_subdistrict",
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "District/City Destination: $des_city",
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Province Destination: $des_province",
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    LocationStamps(previous_locations),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}

class LocationStamps extends StatelessWidget {
  List<dynamic> previous_locations;

  LocationStamps(this.previous_locations);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Location Update Stamp",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w700,
            letterSpacing: -0.8,
          ),
        ),
        Scrollbar(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: DataTable(
              columnSpacing: 10,
              columns: [
                DataColumn(
                  label: Text(
                    "Status",
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                ),
                DataColumn(
                  label: Text(
                    "Past Urban Village",
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                ),
                DataColumn(
                    label: Text(
                  "Past Sub-District",
                  style: TextStyle(
                    fontSize: 12,
                  ),
                )),
                DataColumn(
                    label: Text(
                  "Past District/City",
                  style: TextStyle(
                    fontSize: 12,
                  ),
                )),
                DataColumn(
                    label: Text(
                  "Past Province",
                  style: TextStyle(
                    fontSize: 12,
                  ),
                )),
                DataColumn(
                    label: Text(
                  "Time Stamp",
                  style: TextStyle(
                    fontSize: 12,
                  ),
                )),
              ],
              rows: [
                for (int i = 0; i < previous_locations.length; i++)
                  DataRow(cells: [
                    DataCell(Text(previous_locations[i]["status"])),
                    DataCell(Text(previous_locations[i]["kelurahan"])),
                    DataCell(Text(previous_locations[i]["kecamatan"])),
                    DataCell(Text(previous_locations[i]["kabupaten_kota"])),
                    DataCell(Text(previous_locations[i]["provinsi"])),
                    DataCell(Text(previous_locations[i]["time_stamp"])),
                  ]),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class PackageInfo extends StatelessWidget {
  Map<dynamic, dynamic> bentuk_bantuan;
  Map<dynamic, dynamic> satuan_bantuan;
  int total_receiver;

  PackageInfo(this.bentuk_bantuan, this.satuan_bantuan, this.total_receiver);

  @override
  Widget build(BuildContext context) {
    List<String> tipe_bantuan = [];
    bentuk_bantuan.forEach((key, value) {
      tipe_bantuan.add(key);
    });

    return Container(
        width: 0.83 * displayWidth(context),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: Offset(2.0, 2.0),
            )
          ],
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 0.83 * displayWidth(context),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  topRight: Radius.circular(15.0),
                ),
                color: Colors.lightBlue[100],
              ),
              padding: EdgeInsets.all(15),
              child: Text(
                "Package Information",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  letterSpacing: -0.8,
                  fontSize: 20,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 15),
              child: Text(
                "Value per Receiver ($total_receiver citizen(s))",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
                color: Colors.white,
              ),
              padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: DataTable(
                columns: [
                  DataColumn(label: Text("Type")),
                  DataColumn(label: Text("Value")),
                  DataColumn(label: Text("Unit")),
                ],
                rows: [
                  for (int i = 0; i < tipe_bantuan.length; i++)
                    DataRow(cells: [
                      DataCell(Text(tipe_bantuan[i])),
                      DataCell(
                          Text(bentuk_bantuan[tipe_bantuan[i]].toString())),
                      DataCell(Text(satuan_bantuan[tipe_bantuan[i]])),
                    ])
                ],
              ),
            )),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 15),
              child: Text(
                "Total Value in Bansos Group",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              width: 0.83 * displayWidth(context),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                ),
                color: Colors.white,
              ),
              padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: DataTable(
                columns: [
                  DataColumn(label: Text("Type")),
                  DataColumn(label: Text("Value")),
                  DataColumn(label: Text("Unit")),
                ],
                rows: [
                  for (int i = 0; i < tipe_bantuan.length; i++)
                    DataRow(cells: [
                      DataCell(Text(tipe_bantuan[i])),
                      DataCell(Text(
                          ((double.parse(bentuk_bantuan[tipe_bantuan[i]])) *
                                  total_receiver)
                              .toString())),
                      DataCell(Text(satuan_bantuan[tipe_bantuan[i]])),
                    ])
                ],
              )
            )
          ),
          ],
        ));
  }
}
