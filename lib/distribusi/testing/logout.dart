import 'package:bansospedulimobile/beranda/myhomepage.dart';
import 'package:bansospedulimobile/distribusi/utilities/sizingModule.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:bansospedulimobile/laporan/testing/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Map<String, dynamic>> logoutUser(User user) async {
  final url = 'https://bansos-peduli.herokuapp.com/user/flutter/logout/';

  try {
    final response = await http.post(
      Uri.parse(url),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({"telephone": user.sessionId}),
    );

    var prefs = await SharedPreferences.getInstance();
    await prefs.clear();

    return jsonDecode(response.body);
  } catch (error) {
    return {'error': 'Web service is down'};
  }
}

class LogoutPage extends StatefulWidget {
  User user;

  LogoutPage(this.user);

  @override
  _LogoutPageState createState() => _LogoutPageState();
}

class _LogoutPageState extends State<LogoutPage> {
  int counter = 4;
  bool error = false;

  @override
  void initState() {
    super.initState();
    logoutMechanism(context);
  }

  Future<void> logoutMechanism(BuildContext context) async {
    var result = await logoutUser(widget.user);
    if (result["isLoggedOut"] == null || !result["isLoggedOut"]) {
      setState(() {
        error = true;
      });
    }

    for (int i = 3; i > 0; i--) {
      await Future.delayed(Duration(seconds: 1), () {});

      setState(() {
        counter = i;
      });

      if (counter == 1) {
        await Future.delayed(Duration(seconds: 1), () {});
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MyHomePage()));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        title: Row(
          children: const [
            Icon(
              Icons.pin_drop,
            ),
            Text('BansosPeduli'),
          ],
        ),
      ),
      body: Center(
        child: Container(
          margin: EdgeInsets.only(top: 0.3 * displayHeight(context)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              !error
                  ? Text("You have been successfully logged out.")
                  : Text("An error has occurred."),
              Text("Redirecting to login page in"),
              Text("$counter"),
            ],
          ),
        ),
      ),
    );
  }
}
