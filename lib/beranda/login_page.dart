import 'dart:convert';
import 'package:bansospedulimobile/beranda/myhomepage.dart';
import 'package:bansospedulimobile/laporan/core/admin_report_dashboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'dart:async';
import 'package:http/http.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:bansospedulimobile/distribusi/allBansos.dart';
import 'package:bansospedulimobile/distribusi/trackingAdminDashboard.dart';
import 'package:bansospedulimobile/staff/adminInfor.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  Future<User> webServiceLogin(String telephone, String password) async {
    var response = await post(
        Uri.parse("https://bansos-peduli.herokuapp.com/user/flutter/login/"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({"telephone": telephone, "password": password}));

    if (response.statusCode == 200) {
      Map<String, dynamic> userData = jsonDecode(response.body);
      User user = User(
          sessionId: userData["session-id"],
          isAdmin: userData["is_admin"],
          isCitizen: userData["is_citizen"],
          type: userData["is_admin"] ? userData["type"] : null,
          isVerified: userData["is_citizen"] ? userData["is_verified"] : null,
          telephone: userData["telephone"]);

      final prefs = await SharedPreferences.getInstance();

      prefs.setString('sessionId', userData["session-id"]);
      prefs.setBool('isAdmin', userData["is_admin"]);
      prefs.setBool('isCitizen', userData["is_citizen"]);
      prefs.setString('type', userData["is_admin"] ? userData["type"] : '');
      prefs.setBool('isVerified',
          userData["is_citizen"] ? userData["is_verified"] : false);
      prefs.setString('telephone', userData["telephone"]);

      return user;
    } else {
      return Future.error("Incorrect Login");
    }
  }

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _telephone = TextEditingController();
  final TextEditingController _password = TextEditingController();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          foregroundColor: Colors.blue,
          title: const Text("Login"),
        ),
        body: WillPopScope(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Form(
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.blueAccent[400],
                            borderRadius: BorderRadius.circular(5)),
                        margin: const EdgeInsets.only(top: 100),
                        padding: const EdgeInsets.fromLTRB(114, 18, 114, 18),
                        child: const Text(
                          "Login",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                        child: TextFormField(
                          controller: _telephone,
                          decoration: const InputDecoration(
                            hintText: "Telephone",
                            border: OutlineInputBorder(),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
                        child: TextFormField(
                          controller: _password,
                          obscureText: true,
                          decoration: const InputDecoration(
                            hintText: "Password",
                            border: OutlineInputBorder(),
                          ),
                        ),
                      ),
                      SizedBox(
                          width: 100,
                          height: 40,
                          child: ElevatedButton(
                              onPressed: loading
                                  ? null
                                  : () async {
                                      setState(() {
                                        loading = true;
                                      });
                                      try {
                                        User user =
                                            await widget.webServiceLogin(
                                                _telephone.text,
                                                _password.text);

                                        if (user.isCitizen) {
                                          SchedulerBinding.instance!
                                              .addPostFrameCallback((_) {
                                            Navigator.of(context)
                                                .pushAndRemoveUntil(
                                                    MaterialPageRoute<void>(
                                                        builder: (BuildContext
                                                                context) =>
                                                            MyHomePage(
                                                                user: user)),
                                                    (Route<dynamic> route) =>
                                                        false);
                                          });
                                        } else if (user.isAdmin) {
                                          SchedulerBinding.instance!
                                              .addPostFrameCallback((_) {
                                            Navigator.of(context)
                                                .pushAndRemoveUntil(
                                                    MaterialPageRoute<void>(
                                                        builder: (BuildContext
                                                                context) =>
                                                            AdminInformation(
                                                                user)),
                                                    (Route<dynamic> route) =>
                                                        false);
                                          });
                                        }
                                      } catch (e) {
                                        setState(() {
                                          loading = false;
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(const SnackBar(
                                                  content: Text(
                                                      "You think this is your account?")));
                                        });
                                        _telephone.clear();
                                        _password.clear();
                                      }
                                    },
                              child: const Text(
                                "Login",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              )))
                    ],
                  ),
                ),
              ],
            ),
          ),
          onWillPop: () async {
            return !loading;
          },
        ));
  }
}
