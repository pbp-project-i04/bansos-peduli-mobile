// ignore_for_file: must_be_immutable, non_constant_identifier_names
import 'package:bansospedulimobile/distribusi/testing/logout.dart';
import 'package:bansospedulimobile/registration/Citizen_Registration.dart';
import 'package:bansospedulimobile/user/userpage.dart';
import 'package:flutter/material.dart';
import 'login_page.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:bansospedulimobile/laporan/bansos_report.dart';

class MyHomePage extends StatelessWidget {
  User? user;
  MyHomePage({Key? key, this.user}) : super(key: key);

  FetchDataCovid() async {
    var respones =
        await http.get(Uri.parse("https://api.kawalcorona.com/indonesia"));
    var data = jsonDecode(respones.body);

    return data[0];
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.blueAccent[400],
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              const Text(
                "BansosPeduli",
                style: TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.bold,
                ),
              ),
              if (user == null) ...{
                IconButton(
                  alignment: Alignment.center,
                  icon: const Icon(Icons.login),
                  color: Colors.white,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginPage()),
                    );
                  },
                )
              }
            ],
          )),
      drawer: (user == null) ? null : UserDrawer(user: user!),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            if (user != null) ...{
              Container(
                margin: const EdgeInsets.fromLTRB(20, 50, 30, 10),
                child: const Text(
                  "Terima kasih sudah mendaftar dan berpartisipasi dalam program Bansos COVID-19",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  ),
                ),
              ),
            } else ...{
              Container(
                margin: const EdgeInsets.fromLTRB(20, 50, 30, 10),
                child: const Text(
                  "Segera daftar dan berpartisipasi dalam program Bansos COVID-19",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  ),
                ),
              ),
              SizedBox(
                width: 150,
                height: 40,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CitizenRegistration()),
                    );
                  },
                  child: const Text(
                    'Register',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            },
            const SizedBox(height: 60),
            Container(
              width: double.infinity,
              color: Colors.blueAccent[400],
              padding: const EdgeInsets.only(top: 20, bottom: 20),
              child: Column(
                children: [
                  Container(
                    color: Colors.blueAccent[400],
                    padding: const EdgeInsets.only(top: 20, bottom: 20),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 10),
                          child: const Text(
                            "DATA COVID-19 INDONESIA",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 25,
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                            height: 225,
                            width: size.width * 0.8,
                            margin: const EdgeInsets.only(left: 30, right: 30),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: FutureBuilder(
                              future: FetchDataCovid(),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState !=
                                    ConnectionState.done) {
                                  return const Center(
                                    child: SizedBox(
                                      width: 50,
                                      height: 50,
                                      child: CircularProgressIndicator(),
                                    ),
                                  );
                                } else {
                                  var data = snapshot.data as Map;

                                  return Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Column(
                                        children: [
                                          Column(children: <Widget>[
                                            IconButton(
                                              padding: const EdgeInsets.only(
                                                  top: 10, right: 7),
                                              alignment: Alignment.center,
                                              icon: const FaIcon(
                                                  FontAwesomeIcons.plusCircle),
                                              onPressed: () {},
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  20, 0, 30, 0),
                                              child: const Text(
                                                "POSITIF",
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  20, 0, 30, 30),
                                              child: Text(
                                                "${data['positif']}",
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.blueAccent,
                                                ),
                                              ),
                                            ),
                                          ]),
                                          Column(children: <Widget>[
                                            IconButton(
                                              padding: const EdgeInsets.only(
                                                  top: 10, right: 7),
                                              alignment: Alignment.center,
                                              icon: const FaIcon(
                                                  FontAwesomeIcons.solidHeart),
                                              onPressed: () {},
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  20, 0, 30, 0),
                                              child: const Text("SEMBUH",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                  )),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  20, 0, 30, 10),
                                              child: Text(
                                                "${data['sembuh']}",
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.blueAccent,
                                                ),
                                              ),
                                            ),
                                          ]),
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Column(children: <Widget>[
                                            IconButton(
                                              padding: const EdgeInsets.only(
                                                  top: 10),
                                              alignment: Alignment.center,
                                              icon: const FaIcon(
                                                  FontAwesomeIcons.procedures),
                                              onPressed: () {},
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  30, 0, 20, 0),
                                              child: const Text("DIRAWAT",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                  )),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  30, 0, 20, 30),
                                              child: Text(
                                                "${data['dirawat']}",
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.blueAccent,
                                                ),
                                              ),
                                            ),
                                          ]),
                                          Column(children: <Widget>[
                                            IconButton(
                                              padding: const EdgeInsets.only(
                                                  top: 10),
                                              alignment: Alignment.center,
                                              icon: const FaIcon(
                                                  FontAwesomeIcons.skull),
                                              onPressed: () {},
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  30, 0, 20, 0),
                                              child: const Text(
                                                "MENINGGAL",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                    color: Colors.black),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  30, 0, 20, 10),
                                              child: Text(
                                                "${data['meninggal']}",
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.blueAccent,
                                                ),
                                              ),
                                            ),
                                          ]),
                                        ],
                                      ),
                                    ],
                                  );
                                }
                              },
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class UserDrawer extends StatelessWidget {
  User user;
  UserDrawer({required this.user});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          ListTile(
            title: Align(
              child: Text(
                'BansosPeduli',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontFamily: 'Roboto',
                  fontSize: 30,
                  fontWeight: FontWeight.w900,
                ),
              ),
              alignment: Alignment(-1.0, 0),
            ),
          ),
          const Divider(
            color: Colors.grey,
          ),
          if (user.isCitizen) ...{
            ListTile(
                leading: Icon(Icons.account_circle),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UserInformation(user)));
                },
                title: Text(
                  "Your Dashboard",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                )),
            if (user.isVerified!)
              ListTile(
                leading: Icon(Icons.report_gmailerrorred_sharp),
                title: const Text("Report Your Bansos"),
                onTap: () => {
                  Navigator.of(context).push(MaterialPageRoute<void>(
                      builder: (BuildContext context) => BansosReportPage(
                            user: user,
                            fromHomePage: true,
                          )))
                },
              ),
            ListTile(
              leading: Icon(Icons.logout),
              title: const Text("logout"),
              onTap: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute<void>(
                        builder: (BuildContext context) => LogoutPage(user)),
                    (Route<dynamic> route) => false);
              },
            ),
          }
        ],
      ),
    );
  }
}
