// ignore_for_file: file_names

import 'package:flutter/material.dart';
import './myhomepage.dart';

class LoginPage extends StatelessWidget {
    const LoginPage({Key? key}) : super(key : key,);
    @override
    Widget build(BuildContext context) {
		final _formKey = GlobalKey<FormState>();

        return Scaffold(
            appBar: AppBar(
                backgroundColor: Colors.blueAccent[400],
            ),

            body: Form(
				key: _formKey,
				child: Column(
					children: <Widget> [
						Container (
							decoration: BoxDecoration(
								color: Colors.blueAccent[400],
								borderRadius: BorderRadius.circular(5),
							),

							margin: const EdgeInsets.only(top: 100),
							padding: const EdgeInsets.fromLTRB(114,18,114,18),
							child: const Text("Login",
								style: TextStyle(
									fontWeight: FontWeight.bold,
									fontSize: 25,
									color: Colors.white,
								),
							),
						),

						const SizedBox(height: 10),

						Container (
							padding: const EdgeInsets.fromLTRB(50,0,50,0),
							child: TextFormField(
								decoration: const InputDecoration(
									hintText: "Telephone",
									border: OutlineInputBorder(),
								),
								validator: (value) {
									if (value!.isEmpty) {
										return "Enter telephone number!";
									}
								return null;
								},
							),
						),

						const SizedBox(height: 10),

						Container (
							padding: const EdgeInsets.fromLTRB(50,0,50,0),
							child: TextFormField(
								decoration: const InputDecoration(
									hintText: "Password",
									border: OutlineInputBorder(),
								),
								validator: (value) {
									if (value!.isEmpty) {
										return "Enter password!";
									}
								return null;
								},
							),
						),

						const SizedBox(height: 15),

						SizedBox(
							width: 100,
							height: 40,
							child: ElevatedButton(
								onPressed: () {
									if (_formKey.currentState!.validate()) {
										Navigator.push(
											context,
											MaterialPageRoute(builder: (context) => MyHomePage()),
										);
									}
								},
								child: const Text("Submit",
									style: TextStyle(
										fontWeight: FontWeight.bold,
									),
								),
							),
						),
					],
				),
			),
        );
    }
}