// ignore_for_file: file_names

import 'dart:convert';
import 'package:bansospedulimobile/registration/admin_registration.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:bansospedulimobile/beranda/myhomepage.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(CitizenRegistration());

Future<User> registerUser(
    String telephone,
    String password,
    String name,
    String nik,
    String sktmLink,
    String bukuTabunganLink,
    String areaCode) async {
  var response;
  try {
    response = await post(
        Uri.parse(
            "https://bansos-peduli.herokuapp.com/registration/flutter/register/citizen/"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          "telephone": telephone,
          "password": password,
          "name": name,
          "nik": nik,
          "sktmLink": sktmLink,
          "bukuTabunganLink": bukuTabunganLink,
          "areaCode": areaCode,
        }));
  } catch (e) {
    return Future.error("offline");
  }
  if (response.statusCode == 200) {
    Map userData = jsonDecode(response.body);
    User user = User(
        sessionId: userData["session-id"],
        isCitizen: true,
        isAdmin: false,
        telephone: userData["telephone"],
        isVerified: userData["is_verified"]);

    final prefs = await SharedPreferences.getInstance();

    prefs.setString('sessionId', userData["session-id"]);
    prefs.setBool('isAdmin', userData["is_admin"]);
    prefs.setBool('isCitizen', userData["is_citizen"]);
    prefs.setString('type', userData["is_admin"] ? userData["type"] : '');
    prefs.setBool(
        'isVerified', userData["is_citizen"] ? userData["is_verified"] : false);
    prefs.setString('telephone', userData["telephone"]);

    return Future.delayed(Duration(seconds: 2), () => user);
  } else {
    return Future.error("internal");
  }
}

Future<List<Widget>> getAllArea() async {
  try {
    var response = await get(Uri.parse(
        "https://bansos-peduli.herokuapp.com/registration/flutter/get/all/area/"));

    if (response.statusCode == 200) {
      Map areaData = jsonDecode(response.body);
      List<DropdownMenuItem<String>> dropdownItems = [];
      if (areaData.isNotEmpty) {
        areaData.forEach((areaCode, areaName) {
          dropdownItems.add(DropdownMenuItem<String>(
              value: areaCode, child: Text("$areaName")));
        });
      }
      return dropdownItems;
    } else {
      return Future.error("error");
    }
  } catch (e) {
    return Future.error("error");
  }
}

Future<List> getExistingPhoneNumbers() async {
  try {
    var response = await get(Uri.parse(
        "https://bansos-peduli.herokuapp.com/registration/flutter/get/all/phone/"));

    if (response.statusCode == 200) {
      Map area = jsonDecode(response.body);
      List phoneNumbers = area["numbers"];
      return phoneNumbers;
    }
    return Future.error("error");
  } catch (e) {
    return Future.error("error");
  }
}

class CitizenRegistration extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          ),
          toolbarHeight: 50,
          title: const Text('BansosPeduli')),
      body: SingleChildScrollView(child: CitizenRegistrationForm()),
    );
  }
}

// Create a Form widget.
class CitizenRegistrationForm extends StatefulWidget {
  @override
  CitizenRegistrationFormState createState() {
    return CitizenRegistrationFormState();
  }
}

// Create a corresponding State class. This class holds data related to the form.
class CitizenRegistrationFormState extends State<CitizenRegistrationForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  late Future<List> allAreas = Future.wait([getAllArea()]);
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _telephone = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _nik = TextEditingController();
  final TextEditingController _sktmlink = TextEditingController();
  final TextEditingController _bukutabunganlink = TextEditingController();

  bool serverError = false;
  bool submitting = false;
  String? _errorText;
  String? _area;
  String? _duplicatePhone;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    // Build a Form widget using the _formKey created above.
    return WillPopScope(
        child: Center(
          child: Container(
            width: size.width * 0.8,
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: const Text(
                        "Citizen Registration",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xffe6f3f4),
                          fontSize: 33,
                          wordSpacing: 2,
                        ),
                      )),
                  const Text(
                    "\n  Telephone",
                    style: TextStyle(
                      color: Color(0xff2196f3),
                      fontSize: 20,
                      wordSpacing: 2,
                    ),
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue, width: 1.0),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      errorText: _duplicatePhone,
                    ),
                    controller: _telephone,
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return 'Please enter a Telephone Number';
                      }
                      return null;
                    },
                  ),
                  const Text(
                    "  Password",
                    style: TextStyle(
                      color: Color(0xff2196f3),
                      fontSize: 20,
                      wordSpacing: 2,
                    ),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                    ),
                    obscureText: true,
                    controller: _password,
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return 'Please enter a Password';
                      }
                      return null;
                    },
                  ),
                  const Text(
                    "  Name",
                    style: TextStyle(
                      color: Color(0xff2196f3),
                      fontSize: 20,
                      wordSpacing: 2,
                    ),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                    ),
                    controller: _name,
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return 'Please enter your Name';
                      }
                      return null;
                    },
                  ),
                  const Text(
                    "  NIK",
                    style: TextStyle(
                      color: Color(0xff2196f3),
                      fontSize: 20,
                      wordSpacing: 2,
                    ),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                    ),
                    controller: _nik,
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return 'Please enter your NIK';
                      }
                      return null;
                    },
                  ),
                  const Text(
                    "  SKTM Link",
                    style: TextStyle(
                      color: Color(0xff2196f3),
                      fontSize: 20,
                      wordSpacing: 2,
                    ),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                    ),
                    controller: _sktmlink,
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return 'Please enter your SKTM Link';
                      }
                      return null;
                    },
                  ),
                  const Text(
                    "  Buku Tabungan Link",
                    style: TextStyle(
                      color: Color(0xff2196f3),
                      fontSize: 20,
                      wordSpacing: 2,
                    ),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.blue, width: 1.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.grey, width: 1.0),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red, width: 1.0),
                        )),
                    controller: _bukutabunganlink,
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return 'Please enter your Buku Tabungan Link';
                      }
                      return null;
                    },
                  ),
                  const Text(
                    "  Select Area:",
                    style: TextStyle(
                      color: Color(0xff2196f3),
                      fontSize: 20,
                      wordSpacing: 2,
                    ),
                  ),
                  FutureBuilder(
                      future: allAreas,
                      builder: (context, snapshot) {
                        if (snapshot.connectionState != ConnectionState.done) {
                          return Container(
                            child: Text("loading.."),
                          );
                        } else if (snapshot.hasError) {
                          return Container(
                            child: Text("error"),
                          );
                        } else {
                          List rawData = snapshot.data as List;
                          List<DropdownMenuItem<String>> allAreas = rawData[0];
                          _area = allAreas[0].value;
                          return DropdownButtonFormField<String>(
                            decoration: const InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.blue, width: 1.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey, width: 1.0),
                              ),
                            ),
                            value: _area,
                            items: allAreas,
                            onChanged: (String? value) {
                              setState(() {
                                _area = value;
                              });
                            },
                          );
                        }
                      }),
                  Container(
                    margin: EdgeInsets.all(15),
                    child: ElevatedButton(
                      onPressed: submitting
                          ? null
                          : () async {
                              setState(() {
                                submitting = true;
                              });

                              if (_formKey.currentState!.validate()) {
                                // check if there is duplicate phone number

                                List allPhoneNumbers =
                                    await getExistingPhoneNumbers();

                                if (allPhoneNumbers
                                    .contains(_telephone.text.trim())) {
                                  setState(() {
                                    serverError = false;
                                    _errorText = null;
                                    submitting = false;
                                    _duplicatePhone =
                                        "Sorry but this Telephone Number has been Registered";
                                  });
                                } else {
                                  _duplicatePhone = null;
                                  await registerUser(
                                          _telephone.text,
                                          _password.text,
                                          _name.text,
                                          _nik.text,
                                          _sktmlink.text,
                                          _bukutabunganlink.text,
                                          _area!)
                                      .then((user) {
                                    // create User and then pushAndRemoveUntil(MyHomePage(user:uset))
                                    Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute<void>(
                                            builder: (BuildContext context) =>
                                                MyHomePage(
                                                  user: user,
                                                )),
                                        (Route<dynamic> route) => false);
                                  }).catchError((e) {
                                    setState(() {
                                      _duplicatePhone = null;
                                      submitting = false;
                                      serverError = true;
                                    });
                                    if (e == "internal") {
                                      _errorText =
                                          "Citizen Registration is temporarily Suspended";
                                    } else if (e == "offline") {
                                      _errorText = "Server is Offline";
                                    }
                                    return null;
                                  });
                                }
                              } else {
                                setState(() {
                                  submitting = false;
                                });
                              }
                            },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        width: double.infinity,
                        child: const Text(
                          "Register",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0xffe6f3f4),
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                  if (serverError) Text(_errorText!),
                ],
              ),
            ),
          ),
        ),
        onWillPop: () async {
          return submitting;
        });
  }
}
