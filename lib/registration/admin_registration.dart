import 'dart:convert';

import 'package:bansospedulimobile/staff/adminInfor.dart';
import 'package:bansospedulimobile/user/models/user.dart';
import 'package:flutter/material.dart';
import 'package:bansospedulimobile/registration/Citizen_Registration.dart';
import 'dart:async';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<bool> registerAdmin(
    String telephone, String password, String name, String adminType) async {
  dynamic response;
  try {
    response = await post(
        Uri.parse(
            "https://bansos-peduli.herokuapp.com/registration/flutter/register/admin/"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          "telephone": telephone,
          "password": password,
          "name": name,
          "adminType": adminType
        }));
    if (response.statusCode == 201) {
      Map adminData = jsonDecode(response.body);

      return Future.delayed(Duration(seconds: 2), () => true);
    }
  } catch (e) {
    return Future.error("error");
  }
  return Future.error("error");
}

class AdminRegistration extends StatefulWidget {
  const AdminRegistration({Key? key}) : super(key: key);

  @override
  _AdminRegistrationState createState() => _AdminRegistrationState();
}

class _AdminRegistrationState extends State<AdminRegistration> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _telephone = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _name = TextEditingController();
  bool submitting = false;
  String _adminType = "generator";
  String? _duplicatePhoneString;

  bool willPop = true;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(toolbarHeight: 50, title: const Text('BansosPeduli')),
      body: SingleChildScrollView(
        child: WillPopScope(
            child: Center(
              child: Container(
                // create form here
                margin: const EdgeInsets.symmetric(vertical: 10),
                width: size.width * 0.8,
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          margin: const EdgeInsets.only(bottom: 10),
                          decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(10)),
                          width: size.width * 0.8,
                          height: 100,
                          child: const Center(
                            child: Text(
                              "Register an Admin",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold),
                            ),
                          )),
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: "Telephone",
                          errorText: _duplicatePhoneString,
                        ),
                        keyboardType: TextInputType.number,
                        controller: _telephone,
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            return "Please enter a Telephone Number";
                          }
                          return null;
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(hintText: "Password"),
                        obscureText: true,
                        controller: _password,
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            return "Please enter a Password";
                          }
                          return null;
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(hintText: "Name"),
                        controller: _name,
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            return "Please enter a Name";
                          }
                          return null;
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text("Type"),
                      DropdownButtonFormField<String>(
                          value: _adminType,
                          onChanged: (String? value) => {_adminType = value!},
                          items: const [
                            DropdownMenuItem<String>(
                              child: Text("Generator"),
                              value: "generator",
                            ),
                            DropdownMenuItem(
                              child: Text("Verification"),
                              value: "verification",
                            ),
                            DropdownMenuItem(
                              child: Text("Tracker"),
                              value: "tracker",
                            ),
                            DropdownMenuItem(
                              child: Text("Customer Service"),
                              value: "customer-service",
                            ),
                          ]),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 15),
                        width: size.width * 0.8,
                        child: ElevatedButton(
                            onPressed: submitting
                                ? null
                                : () async {
                                    setState(() {
                                      submitting = true;
                                    });
                                    if (_formKey.currentState!.validate()) {
                                      List allPhoneNumbers =
                                          await getExistingPhoneNumbers();

                                      if (allPhoneNumbers
                                          .contains(_telephone.text.trim())) {
                                        setState(() {
                                          _duplicatePhoneString =
                                              "Sorry but this Phone Number has been Registered";
                                          submitting = false;
                                        });
                                      } else {
                                        _duplicatePhoneString = null;
                                        // register
                                        await registerAdmin(
                                                _telephone.text,
                                                _password.text,
                                                _name.text,
                                                _adminType)
                                            .then((isSuccessful) {
                                          if (isSuccessful) {
                                            ScaffoldMessenger.of(context)
                                              ..removeCurrentSnackBar()
                                              ..showSnackBar(const SnackBar(
                                                content: Text(
                                                    "Admin was registered successfully"),
                                                behavior:
                                                    SnackBarBehavior.floating,
                                                backgroundColor: Colors.blue,
                                              ));

                                            setState(() {
                                              submitting = false;
                                            });
                                          }
                                        }).catchError((e) {
                                          setState(() {
                                            submitting = false;
                                          });
                                        });
                                      }
                                    } else {
                                      setState(() {
                                        submitting = false;
                                      });
                                    }
                                  },
                            child: const Text("Register Admin")),
                      )
                    ],
                  ),
                ),
              ),
            ),
            onWillPop: () async {
              return willPop;
            }),
      ),
    );
  }
}
